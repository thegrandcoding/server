﻿''' <summary>
''' Holds information regarding a punishment (warn/kick/mute/ban)
''' Target: Username of punished player
''' Oper: Username of staff
''' Reason: does that need to be explained?
''' Time: Time that the command was performed.
''' Duration: used for mute, how long it lasts for.
''' EndTime: as above, when the mute ends.
''' Type: warn/kick/mute/ban
''' </summary>
Public Class Punishment
    Public Target As String
    Public Oper As String
    Public Reason As String
    Public Time As DateTime
    Public Duration As Integer 'seconds
    Public EndTime As DateTime ' time duration ends.
    Public Type As String

    ''' <summary>
    ''' How long until the punishment does end
    ''' </summary>
    ''' <returns>Timespan, you can use TimeSpan.TotalSeconds() to get seconds (might need to be rounded)</returns>
    Public Function TimeLeft() As TimeSpan
        Return EndTime - DateTime.Now()
    End Function

    ''' <summary>
    ''' Determines whether the punishment (mute) has expired yet.
    ''' </summary>
    ''' <returns>Boolean, true: has expired, false: not expired</returns>
    Public Function HasExpired()
        Dim timePast As TimeSpan = TimeLeft()
        If timePast.TotalSeconds <= 0 Then
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Creates a new punishment with given details
    ''' </summary>
    ''' <param name="_target">User that has recieved the punishment</param>
    ''' <param name="_oper">Administrator that gives the punishment</param>
    ''' <param name="_reason">Reason for punishment</param>
    ''' <param name="_type">Type of punishment: Kick, Ban or Mute</param>
    ''' <param name="_duration">How long (IN SECONDS) the punishment lasts.</param>
    ''' <param name="_time">When the punishment took place.</param>
    Public Sub New(_target As String, _oper As String, _reason As String, _type As String, Optional _duration As Integer = 0, Optional _time As DateTime = Nothing)
        Target = _target
        Oper = _oper
        Reason = _reason
        If _time = Nothing Then _time = DateTime.Now()
        Time = _time
        Type = _type
        Duration = _duration
        EndTime = Time.AddSeconds(Duration)
    End Sub
End Class
