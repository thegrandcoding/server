﻿Public Class AdminNote
    Public Target As String
    Public Oper As String
    Public IssueDate As DateTime
    Public ID As Integer
    Public Message As String
    Public Sub New(_target As String, _oper As String, _date As DateTime, _msg As String, Optional otherNotes As List(Of AdminNote) = Nothing)
        If otherNotes Is Nothing Then otherNotes = New List(Of AdminNote) ' we calculate the ID based on other notes.
        ID = otherNotes.Count
        Target = _target
        Oper = _oper
        IssueDate = _date
        Message = _msg
    End Sub
    Public Function ToStringFile() As String
        Return ID.ToString + " " + IssueDate.ToString.Replace(" ", "_") + " " + Message.Replace(" ", "&") + " " + Oper
    End Function
    Public Overrides Function ToString() As String
        Return ID.ToString + ": " + IssueDate.ToString + ", " + Message + ", " + Oper
    End Function
End Class
