﻿Imports ChatProgServer.MainApplication
''' <summary>
''' Holds information regarding a votekick
''' Target: user being voted on to kick
''' Reason: reason for the kick
''' yesVoters: names of users that voted yes
''' noVoters: names of users that voted no
''' </summary>
Public Class VoteKick
    Public Target As User
    Public Reason As String
    Private _yesVotes As Integer
    Private _noVotes As Integer
    Public yesVoters As List(Of String)
    Public noVoters As List(Of String)
    Public ReadOnly Property YesVotes As Integer
        Get
            Return _yesVotes
        End Get
    End Property
    Public ReadOnly Property NoVotes As Integer
        Get
            Return _noVotes
        End Get
    End Property
    Public ReadOnly Property MinimumVotesToKick As Integer
        Get
            Dim min As Integer = Math.Ceiling(GetAllLoggedPlayers().Count / 2)
            If min <= 1 Then 'cannot kick if only 1 player says so.
                min = 2
            End If
            Return min
        End Get
    End Property


    Private Sub CheckKick()
        Dim totalUsers As Integer = GetAllLoggedPlayers().Count
        Dim allVotes As Integer = YesVotes - NoVotes
        If allVotes > MinimumVotesToKick Then
            Dim fullReason As String = Reason + "(voted for by: "
            For Each usr As String In yesVoters
                fullReason += usr + ", "
            Next
            fullReason = fullReason.Remove(fullReason.Length - 2, 2) 'removes trailing ', '
            fullReason += ")"
            KickUser(Target.Name, fullReason, "Votekick", Target.Client)
        End If
    End Sub

    Public Sub VoteNo(usr As User)
        If noVoters.Contains(usr.Name) = False Then
            _noVotes += 1
            noVoters.Add(usr.Name)
            Msg(usr.Name + " voted to NOT kick user " + Target.Name + ", " + MinimumVotesToKick.ToString)
            CheckKick()
        Else
            Msg(usr.Name + " attempted to vote again (no)")
        End If
    End Sub

    Public Sub VoteYes(usr As User)
        If yesVoters.Contains(usr.Name) = False Then
            _yesVotes += 1
            yesVoters.Add(usr.Name)
            Msg(usr.Name + " voted to kick user " + Target.Name + ", " + MinimumVotesToKick.ToString)
            CheckKick()
        Else
            Msg(usr.Name + " attempted to vote again (yes)")
        End If
    End Sub

    Public Sub New(trgt As User, reson As String)
        Target = trgt
        Reason = reson
        _yesVotes = 0
        _noVotes = 0
        yesVoters = New List(Of String)
        noVoters = New List(Of String)
    End Sub
End Class
