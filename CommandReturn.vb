﻿''' <summary>
''' Used when returning information from a command
''' Result: Message displayed to the player.
''' ResultColor: Color code of the message
''' BroadCastResult: Message displayed to ALL online players
''' BroadCastColor: Color of the message
''' </summary>
Public Class CommandReturn
    Public BroadCastResult As String
    Public Result As String
    Public BroadCastColor As String
    Public ResultColor As String
    Public DontHandle As Boolean
End Class
