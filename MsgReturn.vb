﻿Public Class MsgReturn
    Public Success As Boolean
    Public Message As String
    Public Color As String
    Public AddtionalReturns As Hashtable
    Public Sub New(suc As Boolean, msg As String, Optional col As String = "&RED&")
        Success = suc
        Message = msg
        Color = col
        AddtionalReturns = New Hashtable
    End Sub
End Class
