Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection
Imports System.Text
Imports System.Windows.Forms
Module MainApplication
    Dim versionNumber As Version = Assembly.GetExecutingAssembly().GetName().Version
    Dim clientsList As New Hashtable
    Private _Log As LogHandle
    Public Users As New Dictionary(Of String, User)
    Public adutyUsers As New List(Of String)
    Friend PossibleAdmins As New Dictionary(Of String, Integer)
    Friend DisallowedNames As New List(Of String)
    Friend BannedUsers As New List(Of Ban)
    Friend TBannedUsers As New List(Of Punishment) ' list of all temp-banned users
    Friend AllowedChars As New List(Of Char)
    Public sendToServer As Boolean = True
    Public rand As New Random
    Public CurrentVoteKick As VoteKick
    Public AvailableIds As List(Of Integer)
    Public LatestClientVersion As Version
    Friend ServerGui As Test_GUI
    Public UsersInUse As Boolean = False
    Public _UsersTyping As New List(Of String)
    Public UsersTypingInUse As Boolean = False
    Public ManagerDutyPlayers As New List(Of User)

    Public MasterList As MasterlistDLL.MasterlistServer

    Public Sub WaitForUsersTyping()
        While UsersTypingInUse
            Threading.Thread.Sleep(1)
        End While
        UsersTypingInUse = True
    End Sub
    Public ReadOnly Property UsersTyping As String
        Get
            WaitForUsersTyping()
            Dim str As String = ""
            If _UsersTyping.Count > 0 Then
                For Each usr As String In _UsersTyping
                    str += usr.ToString() + ","
                Next
                str = str.Remove(str.Length - 1) ' remove trailing ,
            End If
            UsersTypingInUse = False
            Return str
        End Get
    End Property

    ' Options
    Public ShowTBanWarn As Boolean = True
    Public AllowMultipleClients As Boolean = False
    Public AllowCorruptClients As Boolean = False
    Public AllowRegistration As Boolean = True
    Public IsPublicChatFrozen As Boolean = False
    Public IsAdminChatFrozen As Boolean = False
    Public IsALLChatFrozen As Boolean = False

    ' Constants for ranks
    Public Const GUEST As Integer = -1
    Public Const PLAYER As Integer = 0
    Public Const MODERATOR As Integer = 1
    Public Const ADMIN As Integer = 2
    Public Const MANAGER As Integer = 3
    ' Other constants
    Public Const BIGSPACE As String = "                                                "
    Public Const BANNEDPATH As String = "UserSaves/bannedusers.txt"
    Public Const USERSPATH As String = "UserSaves/users.txt"
    Public Const CLIENTVERSIONPATH As String = "https://bitbucket.org/thegrandcoding/client/raw/HEAD/My%20Project/AssemblyInfo.vb"
    Public Const SERVERVERSIONPATH As String = "https://bitbucket.org/thegrandcoding/server/raw/HEAD/My%20Project/AssemblyInfo.vb"
    Public Const REG_SavePath As String = "HKEY_CURRENT_USER\Alex_ChatProgram"

    Public Sub OutSide_SaveLog(reason As String)
        _Log.SaveLog(reason)
    End Sub
    Friend Log_Q As List(Of String) = New List(Of String)
    Public Sub HandleLogQueue()
        While True
            If Log_Q.Count > 0 Then
                _Log.LogMsg(Log_Q.Item(0))
                Log_Q.RemoveAt(0)
                _Log.LogInUse = False
                Threading.Thread.Sleep(5)
            End If
        End While
    End Sub

    Public Sub Log_Outside(mesg As String)
        Log_Q.Add(mesg)
    End Sub


    Public Function SkipLine(line As String) As Boolean
        Return Not IsLineValid(line)
    End Function

    Public Function IsLineValid(line As String) As Boolean
        If String.IsNullOrEmpty(line) Then Return False
        If line.Substring(0, 1) = "#" Then Return False
        If line.Split(" ")(0) = "Sauron" Then Return False
        Return True
    End Function

    Public Function IsPlayerPermBanned(usr As User)
        For Each ban_ As Ban In BannedUsers ' check perm-bans
            If ban_.IsPlayerBanned(usr) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Function GetBanDetails(usr As User) As Ban
        For Each ban_ As Ban In BannedUsers
            If ban_.IsPlayerBanned(usr) Then
                Return ban_
            End If
        Next
        Return Nothing
    End Function

    Public Function GetAllLoggedPlayers(Optional allowUC As Boolean = False) As List(Of User)
        Dim logged As New List(Of User)
        WaitForUsersUse()
        For Each usr As User In Users.Values
            If usr.Name = "Server" Then Continue For
            If usr.IsUnderCover And allowUC = False Then Continue For
            If usr.IsLoggedIn = True Then logged.Add(usr)
        Next
        UsersInUse = False
        Return logged
    End Function

    Public Function GetAllValidLines(path As String) As String()
        Dim str As New List(Of String)
        If Not File.Exists(path) Then Return str.ToArray
        For Each line As String In File.ReadAllLines(path)
            If String.IsNullOrEmpty(line) Then Continue For
            If line.Substring(0, 1) = "#" Then Continue For ' commented
            str.Add(line)
        Next
        Return str.ToArray
    End Function

    Public Function UsersOfSameSerial(oneUser As User)
        Dim allUserLines As String() = GetAllValidLines(USERSPATH)
        Dim usersOfSerial As New List(Of User)
        For Each item As String In allUserLines
            Dim itemSplit As String() = item.Split(" ")
            Dim _name As String = itemSplit(1)
            Dim _date As String = itemSplit(0).Replace("_", " ")
            Dim _pass As String = itemSplit(2)
            Dim _serial As String = itemSplit(3)
            Dim _rank As String = itemSplit(4)
            Dim _user As New User(_name, Nothing, _serial, Nothing, Convert.ToInt32(_rank)) With {
                .IsAllowedByManager = False,
                .Password = _pass
            }
            If Users.ContainsKey(_name) Then
                _user = Users(_name) ' if player is already online, then get that player
                _user.SetRegistration(_date)
                SetUsersValue(_name, _user)
            Else
                _user.SetRegistration(_date)
            End If
            If _name = oneUser.Name Then Continue For ' we want to update everything, but not display it
            If _user.Serial = oneUser.Serial Then
                usersOfSerial.Add(_user)
            End If
        Next
        Return usersOfSerial
    End Function

    Public Function ReadAllPunishments(name As String) As List(Of Punishment)
        Dim path As String = "UserSaves/" + name + "\punishments.txt"
        Dim punishments As New List(Of Punishment)
        If File.Exists(path) = False Then Return punishments
        For Each line As String In File.ReadAllLines(path)
            ' [time] [type] [target] [operator] [reason] [duration]
            Dim lineSplit As String() = line.Split(" ")
            Dim time As DateTime = DateTime.Parse(lineSplit(0).Replace("_", " "))
            Dim type As String = lineSplit(1)
            Dim target As String = lineSplit(2)
            Dim oper As String = lineSplit(3)
            Dim reason As String = lineSplit(4).Replace("&", " ")
            Dim duration As String = lineSplit(5)
            punishments.Add(New Punishment(target, oper, reason, type, Convert.ToInt32(duration), time))
        Next
        Return punishments
    End Function

    Public Sub WriteAllPunishments(usr As User)
        Dim path As String = "UserSaves/" + usr.Name + "/punishments.txt"
        Directory.CreateDirectory("UserSaves/" + usr.Name)
        If File.Exists(path) Then File.Delete(path)
        For Each punish As Punishment In usr.Punishments
            WritePunishmentToFile(punish)
        Next
    End Sub

    Public Sub WritePunishmentToFile(punish As Punishment)
        ' [time] [type] [target] [operator] [reason] [duration]
        Dim str As String = ""
        str += punish.Time.ToString.Replace(" ", "_") + " "
        str += punish.Type + " "
        str += punish.Target + " "
        str += punish.Oper + " "
        str += punish.Reason.Replace(" ", "&") + " "
        str += punish.Duration.ToString + vbCrLf
        File.AppendAllText("UserSaves/" + punish.Target + "/punishments.txt", str)
    End Sub

    Friend Sub UnhandledErrors(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)
        Dim ex As Exception = e.ExceptionObject
        Log_Outside("Error occured and was not handled: " + ex.Message)
        Log_Outside("Error stack: " + ex.StackTrace)
        _Log.SaveLog("UnhandledException-Close")
        Throw ex
    End Sub

    Friend Sub MyApp_ProcessExit(sender As Object, e As ConsoleCancelEventArgs)
        _Log.SaveLog("ProcessExit")
    End Sub
    Dim serverSocket As New TcpListener(Net.IPAddress.Parse("127.0.0.1"), 55001)

    Public Sub SendMessageForce(clientSocket As TcpClient, msg As String)
        Dim broadcastStream As NetworkStream = clientSocket.GetStream()
        broadcastStream.Flush()
        Dim broadcastBytes As [Byte]()
        broadcastBytes = Encoding.UTF8.GetBytes("%" + msg + "$$$")
        broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length)
        broadcastStream.Flush()
    End Sub

    Public Function IsDuplicateUser(usr As User)
        Dim dupl As Boolean = False
        WaitForUsersUse()
        For Each connectedUser As User In Users.Values
            If connectedUser.Name = "Server" Then Continue For
            If connectedUser.Serial = usr.Serial Then
                dupl = True
            ElseIf connectedUser.IP.ToString = usr.IP.ToString Then
                dupl = True
            End If
            If dupl Then
                Msg(connectedUser.Name + " attempted to join with another client (" + usr.Name + ")", True)
                If connectedUser.Name.ToLower.Contains("abdul") Then
                    SendMessageForce(connectedUser.Client, "&RED&YOU CAN ONLY USE ONE CLIENT!!")
                End If
            End If
            If connectedUser.Rank >= 3 Then
                dupl = False
                Msg("Overriden restriction, existing user was a Manager Or higher.", True)
                usr.IsAllowedByManager = True
            ElseIf usr.IsAllowedByManager Then
                dupl = False
                Msg("Overriden, that client was allowed by a manager.")
                usr.IsAllowedByManager = True
            End If
            If AllowMultipleClients = True Then dupl = False
            If connectedUser.Name = usr.Name Then
                dupl = True ' we do this now, as name = all
            End If
            If dupl = True Then Exit For
        Next
        UsersInUse = False
        Return dupl
    End Function

    Private Function NoDisallowedChars(name As String)
        For Each _char As Char In name
            _char = _char.ToString.ToLower()
            If AllowedChars.Contains(_char) Then
                ' ok char
                Continue For
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Function IsDisallowedName(name As String)
        Return name.Contains(" ") Or DisallowedNames.Contains(name) Or (Not NoDisallowedChars(name))
    End Function

    Public Function IsSignificantBehind(ver As Version, cur As Version) As Boolean
        If ver.CompareTo(cur) >= 0 Then Return False ' same version or ahead, so it isnt behind.
        If Math.Abs(ver.Major - cur.Major) > 0 Then
            Return True ' first digit is different
        End If
        If Math.Abs(ver.Minor - cur.Minor) > 0 Then
            Return True ' second digit is different
        End If
        If Math.Abs(ver.Build - cur.Build) > 1 Then
            Return True ' allow some discrepency
        End If
        ' we dont check the last digit, as it is irrelevent.
        Return False 'assume it is not behind

    End Function

    Public Function IsPlayerTBanned(usr As User)
        Dim toRemove As List(Of Punishment) = New List(Of Punishment)
        Dim isBanned As Boolean = False
        For Each tban As Punishment In TBannedUsers ' check temp-bans
            If tban.HasExpired() Then
                toRemove.Add(tban)
                Continue For ' tban has expired, we should add it
            End If
            If tban.Target = usr.Name Then
                isBanned = True
            End If
        Next
        For Each item As Punishment In toRemove
            TBannedUsers.Remove(item)
        Next
        Return isBanned
    End Function

    Public Function GetTBanDetails(usr As User) As Punishment
        Dim pun As Punishment = Nothing
        For Each puns As Punishment In TBannedUsers
            If puns.Target = usr.Name Then
                pun = puns
                Exit For
            End If
        Next
        Return pun
    End Function
    Public ServerIP As IPAddress
    Public ThisComputerIP As IPAddress = Nothing

    Public Sub CloseDownClient(client As TcpClient, Optional reason As String = "Server Request")
        Try
            SendMessageForce(client, "/quit " + reason)
        Catch ex As Exception
        End Try
        Try
            client.Client.Shutdown(SocketShutdown.Both)
            client.Close()
        Catch ex As Exception
        End Try
    End Sub
    Sub newServerHandler()
        Dim clientSocket As New TcpClient
        Dim infiniteCounter As Integer
        Dim counter As Integer
        LatestClientVersion = GetLatestVersion(CLIENTVERSIONPATH)
        Dim attemptIPEP As IPEndPoint
        Dim actAttempt As String
        Try
            serverSocket.Start()
        Catch ex As SocketException
            attemptIPEP = serverSocket.LocalEndpoint
            If attemptIPEP Is Nothing Then
                actAttempt = "[Unable to calculate attempted IP address]"
            Else
                actAttempt = attemptIPEP.Address.ToString()
            End If
            Log_Outside("ERROR DURING SERVER STARTUP: " + ex.ToString())
            MsgBox("Error when starting server: " + ex.Message + vbCrLf + vbCrLf + "Is the port in use?" + vbCrLf + "Attempt IP: " & actAttempt & vbCrLf & "Comp IP: " & GetIPv4Address().ToString & vbCrLf + "NOTICE: Additionally: the server IP address may have been invalid. It will now be reset to this computer's apparant IPv4 address")
            SetOption("IP_Address", GetIPv4Address())
            Log_Outside("ERRUR DURING SERVER STARTUP: Resetting IP address. Could possibly resolve issues")
            Threading.Thread.Sleep(5)
            End
        End Try
        Msg(versionNumber.ToString + ": Chat server started.. " + GetIPv4Address().ToString + ":55001", True)
        Msg("Datetime for Connection.txt: " & DateTime.Now().ToString().Replace(" ", "_"))
        counter = 0
        infiniteCounter = 0
        For infiniteCounter = 1 To 2
            infiniteCounter = 1
            counter += 1
            clientSocket = serverSocket.AcceptTcpClient()
            clientSocket.SendBufferSize = 16384
            Dim bytesFrom(65535) As Byte
            Dim userName As String
            Dim networkStream As NetworkStream
            networkStream = clientSocket.GetStream()
            networkStream.Read(bytesFrom, 0, CInt(clientSocket.ReceiveBufferSize))
            userName = System.Text.Encoding.UTF8.GetString(bytesFrom)
            userName = userName.Substring(0, userName.IndexOf("$$$"))
            ServerGui.UpdateAllGUI()
            Dim userSerial = userName.Split("&")(1)
            Dim userCheckSum = userName.Split("&")(2)
            Dim userVersion As Version = New Version("0.0.0.0")
            Try
                userVersion = New Version(userName.Split("&")(3))
            Catch ex As Exception
                ' client is too old to have version.
            End Try
            userName = userName.Split("&")(0)
            Dim ipend As Net.IPEndPoint = clientSocket.Client.RemoteEndPoint
            Dim usr As New User(userName, ipend.Address, userSerial, clientSocket, -1)
            If IsSignificantBehind(userVersion, LatestClientVersion) Then
                SendMessageForce(clientSocket, "&RED&Your client is out of date, please get the latest version" + BIGSPACE + vbCrLf + userVersion.ToString + " vs." + LatestClientVersion.ToString & vbCrLf & "[No-Reconnect]")
                CloseDownClient(clientSocket)
                Continue For
            End If
            ' check if user serial is valid (essentially, has the user modified it?)
            If (Not EncryptSHA256Managed(userSerial + userCheckSum).Substring(0, 2) = "00") And (AllowCorruptClients = False) Then
                SendMessageForce(clientSocket, "&RED&Your client is corrupt.")
                Msg(userName + "(" + ipend.Address.ToString + ") attempted to connect with invalid serial." & vbCrLf & "[No-Reconnect]")
                CloseDownClient(clientSocket) ' user has modified their serial.
                Continue For
            End If
            If userName.Length > 15 Then
                SendMessageForce(clientSocket, "&RED&Your name is too long" & vbCrLf & "[No-Reconnect]")
                CloseDownClient(clientSocket)
                Continue For
            End If
            ' check is user is already online, or is otherwise invalid name.
            If IsDisallowedName(userName) Then
                SendMessageForce(clientSocket, "&RED&Your name is not allowed" & vbCrLf & "[No-Reconnect]")
                CloseDownClient(clientSocket) ' disallow.
                Continue For
            End If
            If IsDuplicateUser(usr) Then
                SendMessageForce(clientSocket, "&RED&You are already connected." & vbCrLf & "[No-Reconnect]")
                CloseDownClient(clientSocket)
                Continue For
            End If
            If IsPlayerPermBanned(usr) Then
                Dim banDetails As Ban = GetBanDetails(usr)
                SendMessageForce(clientSocket, "&RED&You are banned, double click for details." + BIGSPACE + vbCrLf + "Account banned: " + banDetails.Name + vbCrLf + "Reason: " + banDetails.Reason + vbCrLf + "Banned by: " + banDetails.Oper + vbCrLf + "IP Assosiated: " + banDetails.IP.ToString & vbCrLf & "[No-Reconnect]")
                CloseDownClient(clientSocket) ' close connection
                Continue For 'continue for another player.
            End If
            If IsPlayerTBanned(usr) Then
                Dim tbanDetails As Punishment = GetTBanDetails(usr)
                SendMessageForce(clientSocket, "&RED&You are banned, double click for details" + BIGSPACE + vbCrLf + "This ban is temporary" + vbCrLf + "Account banned: " + tbanDetails.Target.ToString + vbCrLf + "Reason: " + tbanDetails.Reason.ToString + vbCrLf + "Banned by: " + tbanDetails.Oper.ToString + vbCrLf + "Duration: " + tbanDetails.Duration.ToString + vbCrLf + "Expires in " + tbanDetails.TimeLeft.TotalMinutes.ToString + " minutes" & vbCrLf & "[No-Reconnect]")
                Continue For
            End If
            usr.ClientVersion = userVersion
            clientsList(userName) = clientSocket
            SetUsersValue(userName, usr)
            HandleAChat("Guest " + usr.DisplayName + " connected. ", "Server")
            Dim consoleJoinMessage As String = userName + "(" + usr.ID.ToString + ")[" + usr.ClientVersion.ToString + "] connected; " + ipend.Address.ToString + "; " + userSerial
            If PossibleAdmins.ContainsKey(userName) Then
                Msg(consoleJoinMessage + "; Admin?", True, False)
            Else
                Msg(consoleJoinMessage, True, False)
            End If
            Dim client As New HandleClient
            client.StartClient(clientSocket, userName, clientsList)
            ServerGui.UpdateAllGUI()
        Next
        serverSocket.Stop()
        CloseDownClient(clientSocket)
    End Sub

    Public Function TryLogin(unEncrypted As String, encrypted As String) As Boolean
        Dim encryptPass As String = EncryptSHA256Managed(unEncrypted)
        Return encryptPass = encrypted
    End Function

    Public Sub UpdateUserStatus(user As User, Optional dontSetUser As Boolean = False)
        If Not File.Exists(USERSPATH) Then
            Msg("Error during updating admin; file does Not exist.")
            Return
        End If
        If user.Rank <= -1 Then user.Rank = 0
        Dim updated As Boolean = False
        Dim oldText As String() = GetAllValidLines(USERSPATH)
        Dim loopHolder As String() = oldText
        For Each line As String In loopHolder
            If String.IsNullOrEmpty(line) Then Continue For
            Dim index As Integer = Array.IndexOf(loopHolder, line)
            Dim lineSplit As String() = line.Split(" ")
            Dim _date As String = lineSplit(0)
            Dim name As String = lineSplit(1)
            Dim passEncrpyted As String = lineSplit(2)
            Dim serial As String = lineSplit(3)
            Dim rank As Integer = Convert.ToInt16(lineSplit(4))
            If user.Name = name Then
                ' found admin line
                passEncrpyted = user.Password
                rank = user.Rank
                serial = user.Serial
                _date = user.RegistrationDate.ToString
                line = _date.Replace(" ", "_") + " " + name + " " + passEncrpyted + " " + serial + " " + rank.ToString
                oldText(index) = line
                updated = True
                Exit For
            End If
        Next

        If updated = True Then
            File.WriteAllLines(USERSPATH, oldText)
            If dontSetUser = False Then SetUsersValue(user.Name, user)
            Return ' admin was already found, updated.
        End If
        ' admin was not found, need to add
        Dim str As String = user.Name + " " + user.Password + " " + user.Rank.ToString + vbCrLf

        Array.Resize(oldText, oldText.Length + 1)
        oldText(oldText.Length - 1) = str
        File.WriteAllLines(USERSPATH, oldText)
        SetUsersValue(user.Name, user)
    End Sub

    Public Function HandleAdminStart() As Integer
        ' will try to open admins.txt
        ' [date] [name] [password-sha256] [rank]
        ' if not, will create it.
        ' loops thru to see how many admins.
        PossibleAdmins = New Dictionary(Of String, Integer)
        Dim amntOfAdmins As Integer = 0
        Dim _file As String()
        If File.Exists(USERSPATH) Then
            _file = GetAllValidLines(USERSPATH)
            For Each line As String In _file
                If Convert.ToInt16(line.Split(" ")(4)) > 0 Then
                    PossibleAdmins.Add(line.Split(" ")(1), Convert.ToInt32(line.Split(" ")(4)))
                    amntOfAdmins += 1
                End If
            Next
        Else
            Dim obj As Object = File.Create(USERSPATH)
            obj.Dispose()
        End If
    End Function
    Public Function GetLatestVersion(url As String) As Version
        Try
            Dim myReq As HttpWebRequest = WebRequest.Create(url)
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim strResponse As String = resStream.ReadToEnd().ToString()
            Dim location As Integer = strResponse.IndexOf("<Assembly: AssemblyFileVersion(")
            If location >= 0 Then
                Dim getVersion As String = strResponse.Substring(location + "<Assembly: AssemblyFileVersion(".Length + 1, 7)
                Dim latest As Version = New Version(getVersion)
                Return latest
            Else
                Return New Version("0.0.0.0")
            End If
        Catch ex As Exception
            Log_Outside("Error occured: " + ex.Message)
            Log_Outside("Stack trace: " + ex.StackTrace)
        End Try
        Return New Version("0.0.0.0")
    End Function

    Private Sub HandleGUI()
        Dim guiForm As Test_GUI = New Test_GUI()
        ServerGui = guiForm
        Try
            guiForm.ShowDialog()
        Catch ex As InvalidOperationException
            Log_Outside(ex.Message)
        End Try
        ' GUI closed, so we do too.
        Msg("GUI closed: closing server")
        End
    End Sub

    Public Sub SetOption(opt As String, value As Object)
        My.Computer.Registry.SetValue(REG_SavePath + "\Server", opt, value)
        Log_Outside("Created/Updated: " + REG_SavePath.ToString + "\Server\" + opt.ToString + "=" + value.ToString)
        Try
            ServerGui.UpdateAllGUI()
        Catch ex As NullReferenceException
            ' server gui not started
        End Try
    End Sub

    Public Function GetOption(opt As String, defValue As Object) As Object
        Return My.Computer.Registry.GetValue(REG_SavePath + "\Server", opt, defValue)
    End Function

    Public Sub HandleRegistryStuff()
        Dim allowMultiple As Boolean = My.Computer.Registry.GetValue(REG_SavePath + "\Server", "AllowMultiple", False)
        Dim allowCorrupt As Boolean = My.Computer.Registry.GetValue(REG_SavePath + "\Server", "AllowCorrupt", False)
        Dim _showTBanWarn As Boolean = My.Computer.Registry.GetValue(REG_SavePath + "\Server", "TBanWarn", True)
        Dim allowRegis As Boolean = My.Computer.Registry.GetValue(REG_SavePath + "\Server", "AllowRegistration", True)
        Try
            My.Computer.Registry.SetValue(REG_SavePath + "\Server", "AllowMultiple", allowMultiple)
            My.Computer.Registry.SetValue(REG_SavePath + "\Server", "AllowCorrupt", allowCorrupt)
            My.Computer.Registry.SetValue(REG_SavePath + "\Server", "TBanWarn", _showTBanWarn)
            My.Computer.Registry.SetValue(REG_SavePath + "\Server", "AllowRegistration", allowRegis)
        Catch ex As System.Security.SecurityException
            Log_Outside("Access denied to registry: " + ex.Message)
        Catch ex As Exception
            Log_Outside("Unknown error in registry: " + ex.Message)
        End Try
        AllowMultipleClients = allowMultiple
        AllowCorruptClients = allowCorrupt
        ShowTBanWarn = _showTBanWarn
        AllowRegistration = allowRegis
    End Sub

    Public Sub SetUsersValue(name As String, usr As User)
        WaitForUsersUse()
        Users(name) = usr
        UsersInUse = False
    End Sub

    Public Sub CloseServer()
        WaitForUsersUse()
        UsersInUse = False
        For Each usr As User In Users.Values
            If usr.Name = "Server" Then Continue For
            Try
                SendMessageForce(usr.Client, "/quit Server closed")
                usr.Client.Close()
            Catch ex As Exception
            End Try
        Next
        End
    End Sub

    Public Function ResolveAssemblies(sender As Object, args As System.ResolveEventArgs) As System.Reflection.Assembly
        Dim ressourceName = New AssemblyName(args.Name).Name + ".dll"
        Using stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(ressourceName)
            Dim assemblyData(CInt(stream.Length)) As Byte
            stream.Read(assemblyData, 0, assemblyData.Length)
            Return Assembly.Load(assemblyData)
        End Using
    End Function

    ' MAIN FUNCTION HERE
    Sub Main()
        AddHandler AppDomain.CurrentDomain.ProcessExit, AddressOf MyApp_ProcessExit
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledErrors
        AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf ResolveAssemblies
        _Log = New LogHandle(IO.Directory.GetCurrentDirectory + "\logs\", "Server", "ServerLog", versionNumber, My.User.Name)
        Dim guiThread As Threading.Thread = New Threading.Thread(AddressOf HandleGUI)
        guiThread.Name = "Server GUI"
        guiThread.Start()
        Dim serverVerMsg As String = "This server is up-to-date"
        Dim latestServer As Version = GetLatestVersion(SERVERVERSIONPATH)
        If versionNumber.CompareTo(latestServer) = 0 Then
            serverVerMsg = "This server is up-to-date"
        ElseIf versionNumber.CompareTo(latestServer) > 0 Then
            serverVerMsg = "This server is ahead of the latest"
        Else
            ' Auto-download latest version (will be toggleable according to registry)
            ' TODO: toggle via registry of whether this happens.
            MsgBox("Warning:" + vbCrLf + "Your server is out of date" + vbCrLf + "Your version: " + versionNumber.ToString + vbCrLf + "New version: " + latestServer.ToString() & vbCrLf & vbCrLf & "This server will now attempt to download the latest version..")
            Try
                Dim web_Download As New WebClient
                If File.Exists("newServer.exe") Then
                    Dim dlVersion As Version = New Version(FileVersionInfo.GetVersionInfo("newServer.exe").FileVersion)
                    If Not dlVersion = latestServer Then
                        File.Delete("newServer.exe") ' not the latest version
                    End If
                Else
                    web_Download.DownloadFile("https://bitbucket.org/thegrandcoding/server/raw/HEAD/bin/Debug/CP_Server.exe", "newServer.exe")
                End If
                MsgBox("New server has been downloaded. Checking version...") ' we dont tell them of the old version.
                Dim downloadVersion As Version = New Version(FileVersionInfo.GetVersionInfo("newServer.exe").FileVersion)
                If downloadVersion = latestServer Then
                    MsgBox("Version is valid. Running new server now.. this server will close.")
                    Process.Start("newServer.exe")
                    Threading.Thread.Sleep(5)
                    _Log.SaveLog("Closing: new server downloaded & ran")
                    End
                Else
                    _Log.LogError("Downloaded server was of incorrect version, what?")
                    MsgBox("Attempts to download the new version have failed." & vbCrLf & "Please download manually.")
                End If
            Catch ex As Exception
                MsgBox(ex.ToString())
                _Log.LogError(ex.ToString)
            End Try
        End If
        If IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "newServer" Then
            MsgBox("WARNING:" & vbCrLf & "It is advised that you download the complete Visual Studio project of the server." & vbCrLf & "You may ignore this message.")
        End If
        Dim defVal As IPAddress = GetIPv4Address()
        Dim toBeIP As IPAddress
        Try
            toBeIP = GetOption("IP_Address", defVal)
        Catch ex As System.InvalidCastException
            toBeIP = IPAddress.Parse(GetOption("IP_Address", defVal))
        End Try
        ServerIP = toBeIP
        SetOption("IP_Address", ServerIP)
        serverSocket = New TcpListener(ServerIP, 55001)
        TBannedUsers = New List(Of Punishment) ' tbans will be reset when server (re-)starts
        Directory.CreateDirectory("UserSaves") ' creates user saves incase they dont already exist.
        ReplaceItems.Add("&PURPLE&", "")
        ReplaceItems.Add("&BLACK&", "")
        ReplaceItems.Add("&L_RED&", "")
        ReplaceItems.Add("&BLUE&", "")
        ReplaceItems.Add("&L_ORANGE&", "")
        ReplaceItems.Add("&CYAN&", "")
        ReplaceItems.Add("&RED&", "")
        ReplaceItems.Add("&ORANGE&", "")
        ReplaceItems.Add(">>", "")
        ReplaceItems.Add("&Dollar&", "$")
        ReplaceItems.Add("&AndSymbol&", "&")
        For Each item As String In ReplaceItems.Keys
            DisallowedNames.Add(item)
        Next
        AvailableIds = New List(Of Integer)
        DisallowedNames.Add("Server")
        For i As Integer = -1 To 99
            AvailableIds.Add(i)
        Next
        For Each _char As Char In "abcdefghijklmnopqrstuvwxyz0123456789_"
            AllowedChars.Add(_char)
        Next
        BannedUsers = GetBannedPlayers()
        Dim NumAdmins As Integer = HandleAdminStart()
        CurrentVoteKick = Nothing
        Dim clientThread As Threading.Thread = New Threading.Thread(AddressOf newServerHandler)
        clientThread.Start()
        Dim usr As New User("Server", Net.IPAddress.Parse("127.0.0.1"), EncryptSHA256Managed("SauronsLair"), clientsList("Server"), 4)
        Users("Server") = usr
        Dim serverClient As New HandleClient
        serverClient.StartClient(Nothing, "Server", clientsList)
        ' Get latest versions of client and server, to compare against ours (client is kinda useless but idc)
        Msg("There are " + BannedUsers.Count.ToString + " banned users.", True, False)
        Msg("There are " + NumAdmins.ToString + " admins registered.", True, False)
        Msg("Latest server version: " + latestServer.ToString)
        Msg(serverVerMsg)
        Msg("Server is now open and able to receive clients.")
        _Log.LogInUse = False

        Dim logThread As Threading.Thread = New Threading.Thread(AddressOf HandleLogQueue)
        logThread.Name = "Log Thead"
        logThread.Start()
        ServerGui.StartGUI()
        HandleRegistryStuff()

        MasterList = New MasterlistDLL.MasterlistServer(False, MasterlistDLL.MasterList_GameType.Alex_ChatServer)
        AddHandler MasterList.RecieveMessage, AddressOf MasterlistReceiveMessage
        AddHandler MasterList.LogMessage, AddressOf MasterListLog
        MasterList.HostServer("Testing Server", Guid.NewGuid(), False)
        ' Check every ~5 seconds to see which clients are still connected.
        While True
            Threading.Thread.Sleep(5000)
            _Log.LogMsg("Testing clients connections.")
            WaitForUsersUse()
            For Each _usr As User In Users.Values
                If _usr.Name = "Server" Then Continue For
                _Log.LogMsg("Testing " & _usr.DisplayName & " connection.")
                Try
                    SendMessageForce(_usr.Client, "&IGNORE&")
                    If _usr.Client.Connected Then
                        _Log.LogMsg("Test for " & _usr.DisplayName & " succeeded")
                    Else
                        Throw New Exception("Manual exception; client not connected after SendMessageForce")
                    End If
                Catch ex As Exception
                    _Log.LogMsg("Connection failed; removing user. (ex:" & ex.ToString())
                    RemoveUser(_usr.Name, _usr.ID)
                End Try
            Next
            UsersInUse = False
        End While
    End Sub
    ' MAIN FUNCTION HERE

    Private Sub MasterlistReceiveMessage(sender As Object, e As MasterlistDLL.ReceiveMessageEventArgs)
        Msg("MASTERLIST: from " + e.LastOperation + ": " + e.Message, True, False)
    End Sub

    Private Sub MasterListLog(sender As Object, message As String)
        Msg("MASTERLIST: " + message)
    End Sub

    Private Function DateAndTime() As String
        Return "[" + DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + "]"
    End Function
    Dim ReplaceItems As New Dictionary(Of String, String)

    Sub Msg(ByVal mesg As String, ByVal Optional serverBased As Boolean = False, Optional _replaceItems As Boolean = True)
        mesg.Trim()
        If _replaceItems Then
            For Each key As String In ReplaceItems.Keys
                mesg = mesg.Replace(key, ReplaceItems(key))
            Next
        End If
        Dim msgToSend As String = ""
        If serverBased = True Then
            If sendToServer = True Then
                msgToSend = DateAndTime() + " >> " + mesg
            Else
                For Each _manager As User In UsersOfRank(MANAGER)
                    SendMessageToClient(DateAndTime() + " >> " + mesg, _manager.Name, False, True)
                Next
            End If
        Else
            If sendToServer = True Then
                msgToSend = DateAndTime() + " " + mesg
            Else
                For Each _manager As User In UsersOfRank(MANAGER)
                    SendMessageToClient(DateAndTime() + " " + mesg, _manager.Name, False, True)
                Next
            End If
        End If
        Console.WriteLine(msgToSend)
        Log_Outside(msgToSend)
        Try
            ServerGui.GUIAddNewMessage(msgToSend)
            ServerGui.UpdateAllGUI()
        Catch ex As System.NullReferenceException
            ' not always set first time
        End Try
    End Sub

    Public Function GetAllAddresses() As List(Of IPAddress)
        Dim ips As List(Of IPAddress) = New List(Of IPAddress)
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)
        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            ips.Add(ipheal)
        Next
        Return ips
    End Function

    Private Function GetIPv4Address() As Net.IPAddress
        If ThisComputerIP IsNot Nothing Then Return ThisComputerIP
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)
        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                ThisComputerIP = ipheal
                Return ipheal
            End If
        Next
        Return Nothing
    End Function

    Private Function GetDetails_Serial(user As String) As String
        Try
            Return Users(user).Serial
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function GetDetails_IP(user As String) As Net.IPAddress
        Try
            Return Users(user).IP
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function EncryptSHA256Managed(ByVal ClearString As String) As String
        Dim uEncode As New UnicodeEncoding()
        Dim bytClearString() As Byte = uEncode.GetBytes(ClearString)
        Dim sha As New _
        System.Security.Cryptography.SHA256Managed()
        Dim hash() As Byte = sha.ComputeHash(bytClearString)
        Return Convert.ToBase64String(hash)
    End Function

    Public Sub RemoveUser(name As String, usrId As Integer)
        AvailableIds.Add(Users(name).ID)
        AvailableIds.Sort()
        clientsList.Remove(name)
        WaitForUsersUse()
        Users.Remove(name)
        UsersInUse = False
        If _UsersTyping.Contains(name) Then _UsersTyping.Remove(name)
    End Sub

    Friend Sub MuteUser(name As String, reason As String, oper As String, client As TcpClient, duration As Integer)
        If name = "Server" Then Return
        WaitForUsersUse()
        Users(name).Punish(oper, reason, "Mute", duration)
        UsersInUse = False
        Broadcast("&RED&" + name + " was muted by " + oper + " for: " + reason, oper, False, True)
        Msg(name + " was muted by " + oper + " for: " + reason + " " + Users(name).IsMuted.ToString, True)
    End Sub

    Friend Sub WarnUser(name As String, reason As String, oper As String, client As TcpClient)
        If name = "Server" Then Return
        WaitForUsersUse()
        Users(name).Punish(oper, reason, "Warn")
        UsersInUse = False
        Broadcast("&RED&" + name + " was warned by " + oper + " For: " + reason, oper, False, True)
        Msg(name + " was warned by " + oper + " for: " + reason, True)
    End Sub

    Public Sub TBanUser(name As String, reason As String, oper As String, duration As String, client As TcpClient)
        If name = "Server" Then Return
        Dim durInt As Integer = Convert.ToInt32(duration) * 60 ' times 60 to convert minutes to seconds
        WaitForUsersUse()
        Users(name).Punish(oper, reason, "Tban", durInt)
        UsersInUse = False
        Broadcast("&RED&" + name + " was tbanned by " + oper + " for " + duration + " minutes, for " + reason, oper, False, True)
        Msg(name + " was tbanned by " + oper + " for " + duration + " minutes, for " + reason, True)
        SendMessageForce(client, "/quit Tban")
        client.Client.Shutdown(SocketShutdown.Both)
        RemoveUser(name, Users(name).ID)
    End Sub

    Public Sub KickUser(name As String, reason As String, oper As String, client As TcpClient)
        If name = "Server" Then Return
        WaitForUsersUse()
        Users(name).Punish(oper, reason, "Kick")
        UsersInUse = False
        Broadcast("&RED&" + name + " was kicked by " + oper + " For: " + reason, oper, False, True)
        Msg(name + " was kicked by " + oper + " for: " + reason, True)
        SendMessageForce(client, "/quit Kicked")
        client.Client.Shutdown(SocketShutdown.Both)
        RemoveUser(name, Users(name).ID)
    End Sub

    Friend Function GetBannedPlayers() As List(Of Ban)
        ' [isSerial] [isIP] [name] [reason] [oper] [ip] [!serial] 
        Dim bans As New List(Of Ban)
        If File.Exists(BANNEDPATH) = False Then Return bans
        Dim lines() As String = GetAllValidLines(BANNEDPATH)
        Dim lineArray As New ArrayList()
        For x As Integer = 0 To lines.GetUpperBound(0)
            Dim line As String = lines(x)
            Dim lineSplit As String() = line.Split(" ")
            Dim uIsSerial As Boolean = Convert.ToBoolean(lineSplit(0))
            Dim uIsIP As Boolean = Convert.ToBoolean(lineSplit(1))
            Dim uName As String = lineSplit(2)
            Dim uReason As String = lineSplit(3).Replace("&", " ")
            Dim uOper As String = lineSplit(4)
            Dim uIP As IPAddress = IPAddress.Parse(lineSplit(5))
            Dim uSerial As String = lineSplit(6)
            Dim newBan As New Ban(uName, uIP, uReason, uOper, uSerial) With {
                .IsSerialBan = uIsSerial,
                .IsIPBan = uIsIP
            }
            bans.Add(newBan)
        Next
        Return bans
    End Function

    Private Function ReturnBanAsString(bann As Ban) As String
        ' [isserial] [isIP] [name] [reason] [oper] [ip] [serial]
        Dim str As String = ""
        str += bann.IsSerialBan.ToString + " "
        str += bann.IsIPBan.ToString + " "
        str += bann.Name + " "
        str += bann.Reason.Replace(" ", "&") + " "
        str += bann.Oper + " "
        str += bann.IP.ToString + " "
        str += bann.Serial
        Return str
    End Function

    Private Sub SaveBannedPlayer(bannedUser As Ban)
        If BannedUsers.Contains(bannedUser) = False Then
            BannedUsers.Add(bannedUser)
        End If
        File.AppendAllText(BANNEDPATH, ReturnBanAsString(bannedUser) + vbCrLf)
    End Sub

    Friend Sub BanUser(name As String, reason As String, oper As String, client As TcpClient)
        If name = "Server" Then Return
        Dim userIP As Net.IPEndPoint = client.Client.RemoteEndPoint
        If userIP.Address.ToString = "127.0.0.1" Then
            ' we cannot ban the localhost IP address
            SendMessageToClient("&RED&Error: invalid ban request.", oper, False)
            'Return
        End If
        Dim targetUser As User = Users(name)
        targetUser.Punish(oper, reason, "Ban")
        Broadcast("&RED&" + name + " was banned by " + oper + " for: " + reason, oper, False, True)
        Msg(name + " was banned by " + oper + " for: " + reason, True)
        Dim newBan As New Ban(name, userIP.Address, reason, oper, targetUser.Serial)
        SaveBannedPlayer(newBan)
        SendMessageForce(client, "/quit Banned")
        client.Client.Shutdown(SocketShutdown.Both)
        RemoveUser(name, Users(name).ID)
    End Sub

    Public Sub WaitForUsersUse()
        While UsersInUse = True
            Threading.Thread.Sleep(1)
        End While
        UsersInUse = True
    End Sub

    Public Function UsersAtleastRank(minRank As Integer, Optional allowServer As Boolean = False, Optional ignoreUC As Boolean = True) As List(Of User)
        Dim _users As New List(Of User)
        WaitForUsersUse()
        For Each usr As User In Users.Values
            If usr.Name = "Server" And allowServer = False Then Continue For
            If usr.IsUnderCover And ignoreUC = False Then Continue For
            If usr.Rank >= minRank Then _users.Add(usr)
        Next
        UsersInUse = False
        Return _users
    End Function

    Public Function UsersOfRank(rank As Integer, Optional ignoreUC As Boolean = True) As List(Of User)
        Dim _users As New List(Of User)
        WaitForUsersUse()
        For Each usr As User In Users.Values
            If usr.Name = "Server" Then Continue For
            If usr.IsUnderCover And ignoreUC = False Then Continue For
            If usr.Rank = rank Then _users.Add(usr)
        Next
        UsersInUse = False
        Return _users
    End Function

    Public Sub SendMessageToRank(rank As Integer, message As String, Optional allowHigherRanks As Boolean = True, Optional overRideArrows As Boolean = False)
        Dim usersToSend As List(Of User)
        If allowHigherRanks = True Then
            usersToSend = UsersAtleastRank(rank)
        Else
            usersToSend = UsersOfRank(rank)
        End If
        For Each usr As User In usersToSend
            SendMessageToClient(message, usr.Name, False, False)
        Next
    End Sub

    Public Sub HandleAChat(ByVal message As String, ByVal user As String)
        message = "&ORANGE&" + user + " aChat: " + message
        SendMessageToRank(MODERATOR, message, True, True)
    End Sub

    Friend Function ReturnPlayers(Optional userRank As Integer = 0) As String
        Dim str As String = "Connected players:" + vbCrLf
        str += "ID                Name"
        Dim allLoggedPlayers As List(Of User) = GetAllLoggedPlayers(True)
        If allLoggedPlayers.Count = 0 Then
            str = "There are no players online." + vbCrLf
        Else
            For Each usr As User In allLoggedPlayers
                If usr.IsUnderCover Then
                    If userRank < 3 Then
                        Continue For
                    Else
                        str += vbCrLf
                        str += "[UC]"
                    End If
                Else
                    str += vbCrLf
                End If
                str += usr.ID.ToString + "              "
                If usr.ID < 1000 Then str += " "
                str += usr.Name
            Next
        End If
        str += vbCrLf
        Dim guests As List(Of User) = UsersOfRank(GUEST)
        If userRank > 0 And guests.Count > 0 Then
            str += "[ADMIN] Guests connected:" + vbCrLf
            For Each usr As User In guests
                str += usr.DisplayName + ", "
            Next
        End If
        Return str
    End Function

    Public Function SendMessageToClient(ByVal message As String, ByVal uName As String, userMessage As Boolean, Optional overRidePurple As Boolean = False, Optional ByRef mesg As Message = Nothing)
        If uName = "Server" Then
            Msg(message, Not userMessage)
            Return True
        End If
        Dim Item As DictionaryEntry
        For Each Item In clientsList
            If Not Item.Key = uName Then
                Continue For
            End If
            Dim broadcastSocket As TcpClient
            broadcastSocket = CType(Item.Value, TcpClient)
            broadcastSocket.SendBufferSize = 16384
            Dim broadcastStream As NetworkStream = broadcastSocket.GetStream()
            broadcastStream.WriteTimeout = 500
            broadcastSocket.SendTimeout = 501
            Dim broadcastBytes As [Byte]()
            If userMessage = True Then
                broadcastBytes = Encoding.UTF8.GetBytes("%&BLACK& " + uName + ": " + message + "$$$")
            Else
                If overRidePurple = True Then
                    broadcastBytes = Encoding.UTF8.GetBytes("%" + message + "$$$")
                Else
                    broadcastBytes = Encoding.UTF8.GetBytes("%" + "&PURPLE&>> " + message + "$$$")
                End If
            End If
            broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length)
            broadcastStream.Flush()
            Return True
        Next
        Return False
    End Function

    Friend Sub Broadcast(ByVal message As String, ByVal uName As String, ByVal userMessage As Boolean, Optional sendToGuests As Boolean = False)
        Dim usersToSend As List(Of User)
        If sendToGuests = True Then
            usersToSend = UsersAtleastRank(GUEST)
        Else
            usersToSend = UsersAtleastRank(PLAYER)
        End If
        For Each usr As User In usersToSend
            If usr.DoesSeePublic = False And userMessage = True Then
                If Not usr.Rank = -1 Then
                    Continue For
                End If
            End If
            Dim broadcastSocket As TcpClient
            broadcastSocket = usr.Client
            Dim broadcastStream As NetworkStream = broadcastSocket.GetStream()
            Dim broadcastBytes As [Byte]() = Encoding.UTF8.GetBytes("")
            If userMessage = True Then
                broadcastBytes = Encoding.UTF8.GetBytes("%&BLACK&" + uName + ": " + message + "$$$")
            Else
                broadcastBytes = Encoding.UTF8.GetBytes("%&PURPLE&>> " + message + "$$$")
            End If
            If message.Contains("&TYPING&") Then
                broadcastBytes = Encoding.UTF8.GetBytes("%" + message + "$$$")
            End If
            broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length)
            broadcastStream.Flush()
        Next
    End Sub

    Public Sub UpdateBan(name As String, isSerial As Boolean, isIP As Boolean)
        Dim oldText As String() = GetAllValidLines(BANNEDPATH)
        Dim loopHolder As String() = oldText
        For Each line As String In loopHolder
            If String.IsNullOrEmpty(line) Then Continue For
            Dim index As Integer = Array.IndexOf(loopHolder, line)
            Dim lineSplit As String() = line.Split(" ")
            Dim bserial As String = lineSplit(0)
            Dim bip As String = lineSplit(1)
            Dim _name As String = lineSplit(2)
            Dim reason As String = lineSplit(3)
            Dim oper As String = lineSplit(4)
            Dim ip As String = lineSplit(5)
            Dim serial As String = lineSplit(6)
            If name = _name Then
                ' found admin line
                line = isSerial.ToString + " " + isIP.ToString + " " + _name + " " + reason + " " + oper + " " + ip + " " + serial
                oldText(index) = line
                Exit For
            End If
        Next
        File.WriteAllLines(BANNEDPATH, oldText)
    End Sub
End Module
