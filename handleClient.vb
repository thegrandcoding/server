﻿Imports ChatProgServer.MainApplication
Imports System.IO
Imports System.Net.Sockets
Imports System.Text
Public Class HandleClient
    Dim clientSocket As TcpClient
    Dim clientName As String
    Dim clientsList As Hashtable
    Public QueueForMessages As New List(Of Message)
    Public ClientUser As User
    Private hasBeenMutedForFlooding As Boolean = False

    Private Sub AddMessageToQueue(ByVal msg As String, ByVal uName As String, userMessage As Boolean, Optional overRidePurple As Boolean = False)
        Dim mesg As New Message With {
            .msg = msg,
            .Name = uName,
            .userMessage = userMessage,
            .overRidePurple = overRidePurple,
            .sent = False
        }
        QueueForMessages.Add(mesg)
    End Sub

    Public Sub TryQueue()
        If QueueForMessages.Count > 0 Then
            Dim mesg As Message = QueueForMessages.Item(0)
            mesg.sent = SendMessageToClient(mesg.msg, mesg.Name, mesg.userMessage, mesg.overRidePurple, mesg)
            QueueForMessages.Item(0) = mesg
        End If
    End Sub

    Public Function DoQueue(client As TcpClient, Optional username As String = "") As Boolean
        If username = "Server" Then Return True
        Return client.Connected
    End Function

    Public Sub AllQueueHandle()
        While DoQueue(clientSocket, clientName) = True
            If QueueForMessages.Count > 0 Then
                If QueueForMessages.Item(0).sent = True Then QueueForMessages.RemoveAt(0)
                TryQueue()
            End If
            Threading.Thread.Sleep(5)
        End While
    End Sub

    Public Sub StartClient(ByVal inClientSocket As TcpClient, ByVal clineNo As String, ByVal cList As Hashtable)
        Me.clientSocket = inClientSocket
        Me.clientName = clineNo
        Me.clientsList = cList
        WaitForUsersUse()
        Users(clientName).hndleClient = Me
        UsersInUse = False
        If Not clineNo = "Server" Then ' we dont want to handle chat messages from server
            Dim ctThread As Threading.Thread = New Threading.Thread(AddressOf DoChat)
            ctThread.Start()
            SendMessageToClient("Please use /login or /register", clientName, False)
            If PossibleAdmins.ContainsKey(clineNo) Then
                AddMessageToQueue("&PURPLE&Are you an admin? Please log in if so", clineNo, False)
            End If
        End If
        ' but we do want to handle it's queue.
        Dim otherThread As Threading.Thread = New Threading.Thread(AddressOf AllQueueHandle)
        otherThread.Start()

        ClientUser = Users(clientName)

    End Sub

    Public Function UsersAsString(lst As List(Of User), Optional seperator As String = " ") As String
        Dim str As String = ""
        If lst.Count = 0 Then Return str
        For Each item As User In lst
            str += item.DisplayName + seperator
        Next
        str = str.Remove(str.Length - 1) ' removes last trailing seperator
        Return str
    End Function

    Public Function ListAsString(lst As List(Of String), Optional seperator As String = " ") As String
        Dim str As String = ""
        If lst.Count = 0 Then Return str
        For Each item As String In lst
            str += item + seperator
        Next
        str = str.Remove(str.Length - 1) ' removes last trailing seperator
        Return str
    End Function

    Public Function HandleCommand(user As String, cmd As String, args As List(Of String)) As CommandReturn
        Dim argsAsString As String = ListAsString(args)
        Dim setUserAtEnd As Boolean = True
        If cmd = "changepass" Then
            Msg(user + ":/changepass &detailsRemoved&")
        ElseIf cmd = "login" Then
            Msg(user + ":/login &detailsRemoved&")
        ElseIf cmd = "register" Then
            Msg(user + ":/register &detailsRemoved&")
        ElseIf cmd = "slogin" Then
            Msg(user + ":/slogin &detailsRemoved&")
        Else
            Msg(user + ":/" + cmd + " " + argsAsString)
            ' also send command to those on mduty
            For Each usr As User In ManagerDutyPlayers
                Log_Outside("About to send to " & usr.Name + "; " + usr.Client.Connected.ToString())
                SendMessageForce(usr.Client, "&L_PURPLE&" + user + ":/" + cmd + " " + argsAsString)
            Next
        End If
        cmd = cmd.ToLower
        ' Now we actually handle the commands!
        Dim returned As New CommandReturn With {
            .Result = "Unknown cmd.",
            .ResultColor = "&L_RED&",
            .BroadCastResult = "",
            .BroadCastColor = "&PURPLE&",
            .DontHandle = False
        }
        Try
            ClientUser = Users(user)
        Catch ex As Exception
            returned.Result = "Error occured"
            Log_Outside(ex.Message)
            Return returned
        End Try

        ' -- Guest Commands -- '
        ' /slogin [password] - silent login for admins
        If cmd = "slogin" Then
            If ClientUser.Rank = -1 Then
                If args.Count = 1 Then
                    If ClientUser.DoesExist() Then
                        If PossibleAdmins.ContainsKey(ClientUser.Name) Then
                            Dim tryRank As Integer = PossibleAdmins(ClientUser.Name)
                            If tryRank >= 2 Then
                                ClientUser.TryLogin(ClientUser.Name, args.Item(0))
                                If ClientUser.IsLoggedIn Then
                                    Dim consoleJoinMessage As String = "->" + ClientUser.Name + " joined the server(silently); " + ClientUser.IP.ToString + "; " + ClientUser.Serial
                                    returned.Result = "Logged in successfully, you are now UC"
                                    returned.ResultColor = "&RED&"
                                    returned.Result += "; rank as: " + ClientUser.RankName
                                    ClientUser.IsUnderCover = True
                                    ServerGui.UpdateAllGUI()
                                Else
                                    returned.Result = "Error: incorrect password."
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

        ' /register [password] [confirm password]
        If cmd = "register" Then
            If ClientUser.Rank = -1 Then
                If AllowRegistration Then
                    If args.Count = 2 Then
                        If args.Item(0) = args.Item(1) Then
                            If Not ClientUser.DoesExist() Then
                                ClientUser.RegisterNew(args.Item(0))
                                HandleAChat("Guest" + ClientUser.ID.ToString + " registered account: " + ClientUser.Name, "Server")
                                Dim str As String = "[PLEASE DOUBLE CLICK ME FOR HELP]" + BIGSPACE + vbCrLf
                                str += "You have just registered to this server, and are now logged in." + vbCrLf
                                str += "You can type into the box to send messages to other players" + vbCrLf
                                str += "To see who else is online, type: /players" + vbCrLf
                                str += "To send a private message to a person: /pm [user] [message]" + vbCrLf + vbCrLf
                                str += "For a list of other commands: /help" + vbCrLf
                                str += "For a list of administrator commands: /adminhelp" + vbCrLf + vbCrLf
                                str += "(There is a rank system in place, so you may not be able to see all commands)"
                                returned.ResultColor = "&BLUE&"
                                returned.Result = "Registered successfully: " + str
                                returned.BroadCastResult = ClientUser.Name + " joined the server"
                                returned.BroadCastColor = "&PURPLE&"
                                SetUsersValue(ClientUser.Name, ClientUser)
                            Else
                                returned.Result = "Error: you have already registered. Use /login password"
                            End If
                        Else
                            returned.Result = "Error: passwords are not the same"
                        End If
                    Else
                        returned.Result = "Usage: /register [password] [confirm password]"
                    End If
                Else
                    returned.Result = "Error: registration is currently disabled."
                End If
            Else
                returned.Result = "Error: you are already logged in"
            End If
        End If

        ' /login [password]
        If cmd = "login" Then
            If ClientUser.Rank = -1 Then
                If args.Count = 1 Then
                    If ClientUser.DoesExist() Then
                        ClientUser.TryLogin(ClientUser.Name, args.Item(0))
                        If ClientUser.IsLoggedIn Then
                            Dim consoleJoinMessage As String = "->" + ClientUser.Name + " joined the server; " + ClientUser.IP.ToString + "; " + ClientUser.Serial
                            If PossibleAdmins.ContainsKey(ClientUser.Name) Then
                                Msg(consoleJoinMessage + "; Admin?", True, False)
                            Else
                                Msg(consoleJoinMessage, True, False)
                            End If
                            returned.BroadCastResult = ClientUser.Name + " joined the server"
                            returned.BroadCastColor = "&PURPLE&"
                            returned.Result = "Logged in successfully"
                            returned.ResultColor = "&BLUE&"
                            If ClientUser.Rank >= 1 Then returned.ResultColor = "&RED&" 'show that we recognise rank.
                            returned.Result += "; rank as: " + ClientUser.RankName
                        Else
                            returned.Result = "Error: incorrect password."
                        End If
                    Else
                        returned.Result = "Error: you are not registered. /register [password] [confirm password]"
                    End If
                Else
                    returned.Result = "Usage: /login [password]"
                End If
            Else
                returned.Result = "Error: you are already logged in"
            End If
        End If

        ' ----- Player Commands ------ '

        ' /block [user]
        If cmd = "block" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count = 1 Then
                Dim name As String = args.Item(0)
                WaitForUsersUse()
                If Users.ContainsKey(name) Then
                    Dim target As User = Users(name)
                    UsersInUse = False
                    Dim blockRes As Boolean = ClientUser.BlockUser(target)
                    If blockRes Then
                        returned.Result = name + " has been blocked from sending you messages."
                    Else
                        returned.Result = name + " is no longer blocked from sending you messages"
                    End If
                    returned.ResultColor = "&BLUE&"
                Else
                    UsersInUse = False
                    returned.Result = "Error: player not found."
                End If
            Else
                returned.Result = "Usage: /block [user]"
            End If
        End If

        ' /shrug [message]
        If cmd = "shrug" And ClientUser.CanPerformCommand(PLAYER) Then
            returned.DontHandle = True
            Broadcast(argsAsString + " ¯\_(ツ)_/¯", ClientUser.Name, True)
        End If

        ' /setp [on/off]
        If cmd = "setp" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count = 1 Then
                If args.Item(0) = "on" Then
                    returned.Result = "Public chat enabled"
                    ClientUser.DoesSeePublic = True
                ElseIf args.Item(0) = "off" Then
                    returned.Result = "Public chat disabled"
                    ClientUser.DoesSeePublic = False
                Else
                    returned.Result = "Usage: /setp [on/off]"
                End If
            Else
                returned.Result = "Usage: /setp [on/off]"
            End If
        End If

        ' /changepass [password]
        If cmd = "changepass" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count = 1 Then
                ClientUser.Password = EncryptSHA256Managed(args.Item(0))
                UpdateUserStatus(ClientUser)
                returned.Result = "Your password was changed."
            Else
                returned.Result = "Usage: /changepass [password]"
            End If
        End If

        ' /help
        If cmd = "help" And ClientUser.CanPerformCommand(PLAYER) Then
            Dim msg As String = "[Double click for commands]" + BIGSPACE + vbCrLf
            msg += "Available commands: (check order, repeat if invalid)" + vbCrLf
            msg += "- /players - lists players online" + vbCrLf
            msg += "- /admins - lists all players online" + vbCrLf
            msg += "- /pm [username] [message] - private message" + vbCrLf
            msg += "- /ask [question] - ask a question to admins" + vbCrLf
            msg += "- /sha256 [message] - displays sha256 encrypt of message" + vbCrLf
            msg += "- /dice - rolls a fair 6-sided dice" + vbCrLf
            msg += "- /changepass [password] - change your password" + vbCrLf
            msg += "- /setp [on/off] - enables or disables public chat" + vbCrLf
            msg += "- /quit - disconnect peacefully" + vbCrLf
            msg += "- /shrug (message) - displays ¯\_(ツ)_/¯ (after an optional message)" + vbCrLf
            msg += "- /help - this display." + vbCrLf
            If ClientUser.CanPerformCommand(MODERATOR) Then
                msg += "- You have access to admin commands: /adminhelp"
            End If
            returned.Result = msg
            returned.ResultColor = "&BLUE&"
        End If

        ' /admins
        If cmd = "admins" Then
            Dim admins As List(Of User) = UsersAtleastRank(MODERATOR, False, ClientUser.Rank = 3)
            Dim str As String = ""
            If admins.Count = 0 Then
                str = "No admins online"
            Else
                str = "[Double click to see the " + admins.Count.ToString + " admins online]" + BIGSPACE + vbCrLf
                Dim managers As List(Of User) = UsersOfRank(MANAGER, ClientUser.Rank = 3)
                Dim admns As List(Of User) = UsersOfRank(ADMIN, ClientUser.Rank = 3)
                Dim moderators As List(Of User) = UsersOfRank(MODERATOR, ClientUser.Rank = 3)
                If moderators.Count > 0 Then
                    str += "Online Moderators: " + UsersAsString(moderators, ", ") + vbCrLf
                End If
                If admns.Count > 0 Then
                    str += "Online Admins: " + UsersAsString(admns, ", ") + vbCrLf
                End If
                If managers.Count > 0 Then
                    str += "Online Managers: " + UsersAsString(managers, ", ") + vbCrLf
                End If
            End If
            returned.Result = str
            returned.ResultColor = "&RED&"
        End If

        ' /players
        If cmd = "players" And ClientUser.CanPerformCommand(PLAYER) Then
            Dim playersOn As List(Of User) = GetAllLoggedPlayers()
            ' loop thru all players, list them
            Dim str As String = "[Double click to see the " + playersOn.Count.ToString + " players online]" + BIGSPACE + vbCrLf
            str += ReturnPlayers(ClientUser.Rank)
            returned.Result = str
            returned.ResultColor = "&PURPLE&"
        End If

        ' /pm [user] [message]
        If cmd = "pm" Then
            If args.Count >= 2 Then
                If user <> args.Item(0) Then
                    If ClientUser.CanPerformCommand(PLAYER) Then
                        If Users.ContainsKey(args.Item(0)) Then
                            ' user exists.
                            Dim targetUser As User = Users(args.Item(0))
                            If targetUser.IsLoggedIn Then
                                If targetUser.CanRecievePM Then
                                    If targetUser.CanRecievePMFrom(ClientUser) Or ClientUser.CanPerformCommand(MODERATOR) Then
                                        args.RemoveAt(0)
                                        Dim message As String = ListAsString(args)
                                        SendMessageToClient("&L_ORANGE&PM to " + targetUser.Name + ": " + message, user, False, True)
                                        SendMessageToClient("&L_ORANGE&PM from " + user + ": " + message, targetUser.Name, False, True)
                                        returned.DontHandle = True
                                    Else
                                        returned.Result = "Error: that player cant recieve that PM (PM is off)"
                                    End If
                                Else
                                    If targetUser.Rank > 0 Then
                                        returned.Result = "Error: that Staff is busy right now. Use /ask instead"
                                        If targetUser.IsUnderCover Then returned.Result = "Error: player not found."
                                    Else
                                            returned.Result = "Error: that player can't recieve that pm (PM is off)"
                                    End If
                                End If
                            Else
                                returned.Result = "Error: player not found."
                            End If

                        Else
                            returned.Result = "Error: player not found."
                        End If
                    Else
                        ' is not logged in
                        If Users.ContainsKey(args.Item(0)) Then
                            Dim targetUser As User = Users(args.Item(0))
                            If targetUser.CanPerformCommand(MODERATOR) Then
                                args.RemoveAt(0)
                                Dim message As String = ListAsString(args)
                                SendMessageToClient("&L_ORANGE&Guest_PM to " + targetUser.Name + ": " + message, user, False, True)
                                SendMessageToClient("&L_ORANGE&Guest_PM from " + user + ": " + message, targetUser.Name, False, True)
                                returned.DontHandle = True
                            Else
                                returned.Result = "Error: you can only pm /admins as a guest."
                            End If
                        Else
                            returned.Result = "Error: player not found."
                        End If
                    End If

                Else
                    returned.Result = "Error: invalid user"
                End If
            Else
                returned.Result = "Usage: /pm [user] [message]"
            End If
        End If

        ' /ask [question]
        If cmd = "ask" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count >= 1 Then
                For Each adm As User In UsersAtleastRank(MODERATOR)
                    SendMessageToClient("&L_RED&" + user + " /ask: " + ListAsString(args), adm.Name, False, True)
                Next
                returned.DontHandle = True
                SendMessageToClient("Your question was sent to " + UsersAtleastRank(MODERATOR).Count.ToString + " admins.", user, False)
            Else
                returned.Result = "Usage: /ask [question]"
            End If
        End If

        ' /sha256 [message]
        If cmd = "sha256" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count >= 1 Then
                returned.Result = "[Double click for SHA256 of '" + ListAsString(args) + "']" + BIGSPACE + vbCrLf + EncryptSHA256Managed(ListAsString(args))
                returned.ResultColor = "&BLUE&"
            Else
                returned.Result = "Usage: /sha256 [message]"
            End If
        End If

        ' /dice
        If cmd = "dice" And ClientUser.CanPerformCommand(PLAYER) Then
            Dim int As Integer = rand.Next(1, 7)
            returned.BroadCastResult = "dice: " + user + " rolls a " + int.ToString()
            returned.BroadCastColor = "&ORANGE&"
            returned.Result = ""
            Msg("dice: " + user + " rolls a " + int.ToString())
        End If

        ' /quit
        If cmd = "quit" Then
            If Not user = "Server" Then
                setUserAtEnd = False
                SendMessageToClient("&RED&Quit handled, good bye.", user, False, True)
                SendMessageForce(ClientUser.Client, "/quit Clientquit")
                clientSocket.Client.Shutdown(SocketShutdown.Both)
                RemoveUser(user, Users(user).ID)
                returned.DontHandle = True
            Else
                returned.Result = "Usage: /close"
            End If
        End If

        ' /votekick [user] [reason]
        If cmd = "votekick" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count >= 2 Then
                If CurrentVoteKick Is Nothing Then
                    If Users.ContainsKey(args.Item(0)) Then
                        Dim targetUser As User = Users(args.Item(0))
                        If targetUser.CanPerformCommand(PLAYER) AndAlso (Not targetUser.CanPerformCommand(MODERATOR)) Then
                            ' is logged in, but you can kick staff
                            args.RemoveAt(0) 'removes user
                            Dim vote_kick As New VoteKick(targetUser, ListAsString(args))
                            vote_kick.VoteYes(ClientUser)
                            CurrentVoteKick = vote_kick
                            returned.Result = "Started votekick against " + CurrentVoteKick.Target.Name
                            returned.BroadCastResult = ClientUser.DisplayName + " has started a vote to KICK user " + CurrentVoteKick.Target.DisplayName + BIGSPACE + vbCrLf + "VOTE: /voteyes | /voteno" + vbCrLf + "Minimum users to kick: " + CurrentVoteKick.MinimumVotesToKick.ToString + vbCrLf + "Reason: " + CurrentVoteKick.Reason
                        Else
                            returned.Result = "Error: invalid player"
                        End If
                    Else
                        returned.Result = "Error: unknown player"
                    End If
                Else
                    returned.Result = "Error: there is already a votekick against " + CurrentVoteKick.Target.Name
                End If
            Else
                returned.Result = "Usage: /votekick [user] [reason]"
            End If
        End If

        ' /voteyes
        If cmd = "voteyes" And ClientUser.CanPerformCommand(PLAYER) Then
            If CurrentVoteKick IsNot Nothing Then
                CurrentVoteKick.VoteYes(ClientUser)
                returned.Result = "Voted to kick " + CurrentVoteKick.Target.DisplayName
            Else
                returned.Result = "There is no current votekick"
            End If
        End If

        ' /voteno
        If cmd = "voteno" And ClientUser.CanPerformCommand(PLAYER) Then
            If CurrentVoteKick IsNot Nothing Then
                CurrentVoteKick.VoteNo(ClientUser)
                returned.Result = "Voted not to kick " + CurrentVoteKick.Target.DisplayName
            Else
                returned.Result = "There is no current votekick"
            End If
        End If

        ' /voteinfo
        If cmd = "voteinfo" And ClientUser.CanPerformCommand(PLAYER) Then
            If CurrentVoteKick IsNot Nothing Then
                Dim str As String = "[Double click for votekick against " + CurrentVoteKick.Target.DisplayName + "]" + BIGSPACE + vbCrLf
                str += "Started by: " + CurrentVoteKick.yesVoters(0) + vbCrLf
                str += "Reason: " + CurrentVoteKick.Reason + vbCrLf
                str += "Yes votes: " + CurrentVoteKick.YesVotes.ToString + vbCrLf
                str += "Users voted yes: " + ListAsString(CurrentVoteKick.yesVoters, ", ") + vbCrLf
                str += "No votes: " + CurrentVoteKick.NoVotes.ToString + vbCrLf
                str += "Users voted no: " + ListAsString(CurrentVoteKick.noVoters, ", ") + vbCrLf
                str += "Current vote-value: " + (CurrentVoteKick.YesVotes - CurrentVoteKick.NoVotes).ToString + vbCrLf
                str += "Minimum votes: " + CurrentVoteKick.MinimumVotesToKick.ToString
                returned.Result = str
            Else
                returned.Result = "There is no current votekick"
            End If
        End If

        If cmd = "setpm" And ClientUser.CanPerformCommand(PLAYER) Then
            If args.Count = 1 Then
                If args.Item(0) = "on" Then
                    ClientUser.CanRecievePM = True
                    returned.Result = "Your PM is now enabled"
                    returned.ResultColor = "&L_ORANGE&"
                Else
                    ClientUser.CanRecievePM = False
                    returned.Result = "Your PM is now disabled"
                End If
            Else
                returned.Result = "Usage: /setpm [on/off]"
            End If
        End If

        ' ----- Moderator Commands ----- '
        ' /adminhelp
        If cmd = "adminhelp" And ClientUser.CanPerformCommand(MODERATOR) Then
            Dim msg As String = "[Double Click for Admin Commands]" + BIGSPACE + vbCrLf
            msg += "- /broadcast [message] - sends message to all" + vbCrLf
            msg += "- /a [message] - sends message to all admins" + vbCrLf
            msg += "- /apm [user] [meassage] - send an admin pm to a user." + vbCrLf
            msg += "- /aduty - toggles admin duty" + vbCrLf
            msg += "- /identity [user] - displays info about user" + vbCrLf
            msg += "- /warn [user] [reason] - warns user for given reason" + vbCrLf
            msg += "- /kick [user] [reason] - kicks user for given reason" + vbCrLf
            msg += "- /alias [user] - lists other accounts on the same computer" + vbCrLf
            msg += "- /see [user] - see punishments made against a user." + vbCrLf
            msg += "- /bans - lists all bans" + vbCrLf
            msg += "- /notes [user] - lists all notes against user." + vbCrLf
            msg += "- /addnote [user] [message] - adds a note against a user"
            If ClientUser.CanPerformCommand(ADMIN) Then
                msg += vbCrLf + "- [A] /ban [user] [reason] - bans a user for given reason" + vbCrLf
                msg += "- [A] /seeban [user] - see full details of a user's ban"
                msg += "- [A] /uc - toggles whether you are hidden from /admins and /players"
            End If
            If ClientUser.CanPerformCommand(MANAGER) Then
                msg += vbCrLf + "- [M] /giverank [user] [rankID] - gives rank of rankID to user." + vbCrLf
                msg += "- [M] /m [message] - manager chat, speak to all managers." + vbCrLf
                msg += "- [M] /close - closes the server." + vbCrLf
                msg += "- [M] /allowmultiple - allows any user to connect with multiple clients" + vbCrLf
                msg += "- [M] /allowcorrupt - toggles whether users can join with corrupted clients" + vbCrLf
                msg += "- [M] /mduty - enter manager duty" + vbCrLf
                msg += "- [M] /newregister [user] [pass] [rank]" + vbCrLf
                msg += "- [M] /deleteuser [name] - removes a user from files (WILL KICK THEM IF ONLINE)" + vbCrLf
                msg += "- [M] /update - reloads bans/admins" + vbCrLf
                msg += "- [M] /forceserial [user] - sends message to client to force them to change serial" + vbCrLf
                msg += "- [M] /updateban [user] [name/serial/ip ban] [true/false] - makes the ban name-based, serial-based or IP-based"
            End If
            returned.Result = msg
            returned.ResultColor = "&BLUE&"
        End If

        ' /broadcast [message] !REQUIRES ADMIN
        If cmd = "broadcast" And ClientUser.CanPerformCommand(MODERATOR) Then
            ' is an admin
            Dim str_ As String = ""
            For Each item As String In args
                str_ += item + " "
            Next
            returned.BroadCastResult = "[BROADCAST] " + str_
            returned.BroadCastColor = "&RED&"
            returned.Result = ""
        End If

        ' /aduty !REQUIRES ADMIN
        If cmd = "aduty" Then
            If ClientUser.CanPerformCommand(MODERATOR) Then
                ' is an admin
                If adutyUsers.Contains(user) Then
                    ' already on aduty
                    adutyUsers.Remove(user)
                    returned.Result = "Off duty"
                    returned.ResultColor = "&L_RED&"
                Else
                    ' not on aduty
                    adutyUsers.Add(user)
                    returned.Result = "On duty"
                    returned.ResultColor = "&L_RED&"
                End If
            End If
        End If

        ' /a [message] !REQUIRES ADMIN
        If cmd = "a" Then
            If ClientUser.CanPerformCommand(MODERATOR) Then
                HandleAChat(argsAsString, user)
                returned.DontHandle = True
            End If
        End If

        ' /warn [user] [reason] !REQURIES ADMIN
        If cmd = "warn" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count >= 2 Then
                If user <> args.Item(0) Then
                    If Users.ContainsKey(args.Item(0)) Then
                        Dim warned As User = Users(args.Item(0))
                        If warned.Rank < ClientUser.Rank Then
                            args.RemoveAt(0)
                            WarnUser(warned.Name, ListAsString(args), user, warned.Client)
                            returned.DontHandle = True
                        Else
                            returned.Result = "Error: invalid player"
                        End If
                    Else
                        returned.Result = "Error: player not found"
                    End If
                Else
                    returned.Result = "Error: cannot warn self"
                End If
            Else
                returned.Result = "Usage: /warn [user] [reason]"
            End If
        End If

        ' /notes [user]
        If cmd = "notes" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count = 1 Then
                If Users.ContainsKey(args.Item(0)) Then
                    Dim targetUser As User = Users(args.Item(0))
                    returned.Result = targetUser.DisplayAllNotes()
                    returned.ResultColor = "&PURPLE&"
                End If
            Else
                returned.Result = "Usage: /notes [user]"
            End If
        End If

        ' /addnote [user] [message]
        If cmd = "addnote" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count >= 2 Then
                If Users.ContainsKey(args.Item(0)) Then
                    Dim targetUser As User = Users(args.Item(0))
                    args.RemoveAt(0)
                    targetUser.AddNote(ListAsString(args), ClientUser.Name)
                    returned.Result = "Note was added"
                End If
            Else
                returned.Result = "Usage: /addntoe [user] [message]"
            End If
        End If

        ' /tban [user] [duration] [reason]
        If cmd = "tban" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count >= 3 Then
                If user <> args.Item(0) Then
                    If Users.ContainsKey(args.Item(0)) Then
                        Dim kicked As User = Users(args.Item(0))
                        If kicked.Rank < ClientUser.Rank Then
                            Dim duration As String = args.Item(1)
                            If Integer.TryParse(duration, 5) = True Then
                                Dim intDur As Integer = Convert.ToInt32(duration)
                                If intDur <= (10 * ClientUser.Rank) Then
                                    args.RemoveAt(0)
                                    args.RemoveAt(0)
                                    TBanUser(kicked.Name, ListAsString(args), ClientUser.Name, duration, kicked.Client)
                                    returned.DontHandle = True
                                Else
                                    returned.Result = "Error: max tban duration: " + (10 * ClientUser.Rank).ToString + " mins"
                                End If
                            Else
                                returned.Result = "Error: invalid duration (it must be integer)"
                            End If
                        Else
                            returned.Result = "Error: invalid player"
                        End If
                    Else
                        returned.Result = "Error: player not found"
                    End If
                Else
                    returned.Result = "Error: invalid player"
                End If
            Else
                returned.Result = "Usage: /tban [user] [duration(mins)] [reason]"
            End If
        End If

        ' /kick [user] [reason] 
        If cmd = "kick" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count >= 2 Then
                If user <> args.Item(0) Then ' dont allow them to kick themselves
                    If Users.ContainsKey(args.Item(0)) Then
                        Dim targetUser As User = Users(args.Item(0))
                        If targetUser.Rank >= ClientUser.Rank Then
                            returned.Result = "Error: invalid player" ' cant kick higher ranks
                        Else
                            ' user attempted to be kicked is online
                            args.RemoveAt(0) ' remove user from args
                            KickUser(targetUser.Name, ListAsString(args), user, targetUser.Client)
                            returned.DontHandle = True
                        End If

                    End If
                Else
                    returned.Result = "Error: invalid player"
                End If
            Else
                returned.Result = "Usage: /kick [user] [reason]"
            End If
        End If
        ' /apm [user] [message] 
        If cmd = "apm" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count >= 2 Then
                If user <> args.Item(0) Then
                    If Users.ContainsKey(args.Item(0)) Then
                        ' user exists.
                        Dim targetUser As User = Users(args.Item(0))
                        args.RemoveAt(0)
                        Dim message As String = ListAsString(args)
                        SendMessageToClient("&L_RED&aPM to " + targetUser.Name + ": " + message, user, False, True)
                        SendMessageToClient("&L_RED&aPM from " + user + ": " + message, targetUser.Name, False, True)
                        returned.DontHandle = True
                    Else
                        returned.Result = "Error: player not found."
                    End If
                Else
                    returned.Result = "Error: invalid player"
                End If
            Else
                returned.Result = "Usage: /apm [user] [message]"
            End If
        End If

        ' /alias [name]
        If cmd = "alias" And ClientUser.CanPerformCommand(MODERATOR) Then
            ' returns a list of all users of the same serial (same computer)
            If args.Count = 1 Then
                If Users.ContainsKey(args.Item(0)) Then
                    Dim targetUser As User = Users(args.Item(0))
                    Dim similarUsers As List(Of User) = UsersOfSameSerial(targetUser)
                    Dim str As String = "[Double click to see " + similarUsers.Count.ToString + " aliases]" + BIGSPACE + vbCrLf
                    ' "                "
                    str += "Registration                  Name                Same password?" + vbCrLf
                    str += targetUser.RegistrationDate + ": " + targetUser.Name + vbCrLf
                    For Each usr As User In similarUsers
                        str += usr.RegistrationDate.ToString + ": " + usr.Name
                        Dim spaceLeft As String = "                               "
                        MsgBox(spaceLeft.Length.ToString)
                        For Each letter As Char In usr.Name
                            spaceLeft = spaceLeft.Remove(0, 2)
                        Next
                        MsgBox(spaceLeft.Length.ToString + vbCrLf + usr.Name.Length.ToString)
                        str += spaceLeft
                        If usr.Password = targetUser.Password Then
                            str += "yes"
                        Else
                            str += "no"
                        End If
                        str += vbCrLf
                    Next
                    returned.Result = str
                    returned.ResultColor = "&BLUE&"
                Else
                    returned.Result = "Error: player not found."
                End If

            Else
                returned.Result = "Usage: /alias [name]"
            End If
        End If

        ' /see [user]
        If cmd = "see" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count = 1 Then
                Dim usr As User
                If Users.ContainsKey(args.Item(0)) Then
                    usr = Users(args.Item(0))
                Else
                    usr = New User(args.Item(0), Net.IPAddress.Parse("127.0.0.1"), "no", Nothing, 0)
                End If
                If usr.Punishments.Count > 0 Then
                    Dim str = "[Double click to see " + usr.Punishments.Count.ToString + " punishments]" + BIGSPACE + vbCrLf
                    For Each punishmnt As Punishment In usr.Punishments
                        str += punishmnt.Time.ToString + ": " + punishmnt.Type + ", " + punishmnt.Reason + "; " + punishmnt.Oper + vbCrLf
                    Next
                    returned.Result = str
                Else
                    returned.Result = "This player is clear."
                End If
            Else
                returned.Result = "Usage: /see [user]"
            End If
        End If

        ' /identity [user] 
        If cmd = "identity" And ClientUser.CanPerformCommand(MODERATOR) Then
            If args.Count = 1 Then
                If Users.ContainsKey(args.Item(0)) And (Not args.Item(0) = "Server") Then ' cannot identity server
                    Dim targetUser As User = Users(args.Item(0))
                    returned.Result = "[Double Click For Information on user]" + BIGSPACE + vbCrLf + "Name:  " + targetUser.Name + vbCrLf + "Rank: " + targetUser.RankName + vbCrLf + "IP Address: " + targetUser.IP.ToString + vbCrLf + "Serial: " + targetUser.Serial.ToString + vbCrLf + "Client version: " + targetUser.ClientVersion.ToString + vbCrLf + "Password: " + targetUser.Password
                    returned.ResultColor = "&BLUE&"
                Else
                    returned.Result = "Error: player not found."
                End If
            Else
                returned.Result = "Usage: /identity [user]"
            End If
        End If

        ' /bans
        If cmd = "bans" And ClientUser.CanPerformCommand(MODERATOR) Then
            BannedUsers = GetBannedPlayers()
            If BannedUsers.Count > 0 Then
                Dim str As String = "[Double click to see " + BannedUsers.Count.ToString + " banned users]" + BIGSPACE + vbCrLf
                For Each bn As Ban In BannedUsers
                    str += "- " + bn.Name + " banned by " + bn.Oper + " for: " + bn.Reason + vbCrLf
                Next
                returned.Result = str
            Else returned.Result = "There are no banned players"
            End If
        End If

        ' ----- Admin Commands ----- '
        ' /mute [user] [duration - seconds] [reason] 
        If cmd = "mute" And ClientUser.CanPerformCommand(ADMIN) Then
            If args.Count >= 3 Then
                If Users.ContainsKey(args.Item(0)) Then
                    Dim targetUser As User = Users(args.Item(0))
                    If targetUser.IsMuted Then
                        returned.Result = "Error: player is already muted."
                    Else
                        Dim duration As Integer = Convert.ToInt32(args.Item(1))
                        args.RemoveAt(0) ' removes user
                        args.RemoveAt(0) ' removes duration
                        MuteUser(targetUser.Name, ListAsString(args, " "), user, targetUser.Client, duration)
                        returned.Result = targetUser.Name + " was muted for " + targetUser._Mute.Duration.ToString + ", expires at: " + targetUser._Mute.EndTime.ToShortTimeString
                    End If
                Else
                    returned.Result = "Error: player not found"
                End If
            Else
                returned.Result = "Usage: /mute [user] [duration (seconds)] [reason]"
            End If
        End If

        ' /uc
        If cmd = "uc" And ClientUser.CanPerformCommand(ADMIN) Then
            If ClientUser.IsUnderCover = True Then
                returned.Result = "You are no longer hidden."
                ClientUser.IsUnderCover = False
            Else
                returned.Result = "You are now hidden."
                ClientUser.IsUnderCover = True
            End If
        End If

        ' /ban [user] [reason] 
        If cmd = "ban" And ClientUser.CanPerformCommand(ADMIN) Then
            If args.Count >= 2 Then
                If user <> args.Item(0) Then
                    Dim username As String = args.Item(0)
                    args.RemoveAt(0)
                    If Users.ContainsKey(username) Then
                        BanUser(username, ListAsString(args), user, Users(username).Client)
                        returned.DontHandle = True
                    Else
                        returned.Result = "Error: player not found."
                    End If
                Else
                    returned.Result = "Error: invalid player"
                End If
            Else
                returned.Result = "Usage: /ban [user] [reason]"
            End If
        End If

        ' /seeban [user]
        If cmd = "seeban" And ClientUser.CanPerformCommand(ADMIN) Then
            If args.Count = 1 Then
                Dim usr As User
                If Users.ContainsKey(args.Item(0)) Then
                    usr = Users(args.Item(0))
                Else
                    usr = New User(args.Item(0), Net.IPAddress.Parse("127.0.0.1"), "no", Nothing, 0)
                End If
                Dim bann As Ban = GetBanDetails(usr)
                If bann IsNot Nothing Then
                    Dim str As String = "[Double click for ban details]" + BIGSPACE + vbCrLf
                    str += "Banned user: " + bann.Name + vbCrLf
                    str += "Operator: " + bann.Oper + vbCrLf
                    str += "Reason: " + bann.Reason + vbCrLf
                    str += "User IP: " + bann.IP.ToString + vbCrLf
                    str += "User serial: " + bann.Serial + vbCrLf
                    str += "> Is IP ban?: " + bann.IsIPBan.ToString + vbCrLf
                    str += "> Is Serial ban?: " + bann.IsSerialBan.ToString
                    returned.Result = str
                Else
                    returned.Result = "Error: that player is not banned."
                End If
            Else
                returned.Result = "Usage: /seeban [user]"
            End If
        End If

        ' /freeze [all/(p)ublic/(a)dmin]
        If cmd = "freeze" And ClientUser.CanPerformCommand(ADMIN) Then
            If args.Count = 1 Then
                If args.Item(0).ToLower = "all" Then
                    If IsALLChatFrozen = True Then
                        IsALLChatFrozen = False
                    Else
                        IsALLChatFrozen = True
                    End If
                    returned.Result = "All chats enabled: " + IsALLChatFrozen.ToString
                ElseIf args.Item(0).ToLower = "public" Or args.Item(0).ToLower = "p" Then
                    If IsPublicChatFrozen = True Then
                        IsPublicChatFrozen = False
                    Else
                        IsPublicChatFrozen = True
                    End If
                    returned.Result = "Public chats enabled: " + IsPublicChatFrozen.ToString
                ElseIf args.Item(0).ToLower = "admin" Or args.Item(0).ToLower = "a" Then
                    If IsAdminChatFrozen = True Then
                        IsAdminChatFrozen = False
                    Else
                        IsAdminChatFrozen = True
                    End If
                    returned.Result = "Admin chats enabled: " + IsAdminChatFrozen.ToString
                End If
            End If
        End If

        ' ----- Manager Commands ----- '

        ' /resetpass [user] [password]
        If cmd = "resetpass" And ClientUser.CanPerformCommand(MANAGER) Then
            If args.Count = 2 Then
                If Users.ContainsKey(args.Item(0)) Then
                    Dim targetUser As User
                    WaitForUsersUse()
                    targetUser = Users(args.Item(0))
                    UsersInUse = False
                    If targetUser.Rank = GUEST Then
                        Dim tempUser As User = New User(targetUser.Name, targetUser.IP, targetUser.Serial, targetUser.Client, targetUser.StoredRank)
                        tempUser.Password = EncryptSHA256Managed(args.Item(1))
                        tempUser.SetRegistration(targetUser.StoredRegister)
                        UpdateUserStatus(tempUser, True)
                        SendMessageToClient("[Your password was reset. Double click]" + BIGSPACE + vbCrLf + "Your new password: " + args.Item(1), args.Item(0), False)
                        returned.Result = "User password was reset"
                    Else
                        returned.Result = "Error: you can only reset a Guest"
                    End If
                Else
                    returned.Result = "Error: player not found"
                End If
            Else
                returned.Result = "Usage: /resetpass [user] [new password]"
            End If
        End If

        '/allowmultiple
        If cmd = "allowmultiple" And ClientUser.CanPerformCommand(MANAGER) Then
            If MainApplication.AllowMultipleClients = True Then
                MainApplication.AllowMultipleClients = False
                SetOption("AllowMultiple", False)
                returned.Result = "Users can now only connect with 1 account"
            Else
                SetOption("AllowMultiple", True)
                MainApplication.AllowMultipleClients = True
                returned.Result = "Users can connect with multiple accounts"
            End If
        End If

        ' /allowcorrupt
        If cmd = "allowcorrupt" And ClientUser.CanPerformCommand(MANAGER) Then
            If MainApplication.AllowCorruptClients = True Then
                SetOption("AllowCorrupt", False)
                MainApplication.AllowCorruptClients = False
                returned.Result = "Users must have a valid serial"
            Else
                SetOption("AllowCorrupt", True)
                MainApplication.AllowCorruptClients = True
                returned.Result = "Users can join with invalid serial"
            End If
        End If

        ' /deleteuser [user]
        If cmd = "deleteuser" And ClientUser.CanPerformCommand(MANAGER) Then
            If args.Count = 1 Then
                Dim targetUser As User
                Dim dokickUser As Boolean = False
                If Users.ContainsKey(args.Item(0)) Then
                    targetUser = Users(args.Item(0))
                    dokickUser = True
                Else
                    targetUser = New User(args.Item(0), Nothing, Nothing, Nothing, Nothing, True)
                End If
                If targetUser.DoesExist() Then
                    targetUser.DeletePlayer()
                    returned.Result = "Removed " + targetUser.Name + " from files."
                    If dokickUser Then KickUser(targetUser.Name, "removed", "Server", targetUser.Client)
                Else
                    returned.Result = "That player did not exist in the first place."
                End If
            Else
                returned.Result = "Usage: /deleteuser [name]"
            End If
        End If

        ' /close
        If cmd = "close" And args.Count = 0 Then
            If ClientUser.CanPerformCommand(MANAGER) Then
                Msg("Server closed by admin " + user, True)
                Broadcast("&RED&Server closed by admin: " + user & BIGSPACE & vbCrLf & "[No-Reconnect]", user, False)
                returned.DontHandle = True
                MainApplication.CloseServer()
            End If
        End If

        ' /giverank [user] [rankID]
        If cmd = "giverank" And ClientUser.CanPerformCommand(MANAGER) Then
            If args.Count = 2 Then
                If (Convert.ToInt16(args.Item(1)) >= 0) And (Convert.ToInt16(args.Item(1)) <= 3) Then
                    Dim targetUser As User = Users(args.Item(0))
                    If Not targetUser.Name = ClientUser.Name Then
                        targetUser.Rank = args.Item(1)
                        UpdateUserStatus(targetUser)
                        HandleAdminStart() 'resets possible admins.
                        returned.Result = targetUser.DisplayName + " is now rank: " + Users(targetUser.Name).RankName
                    Else
                        returned.Result = "Error: You cannot change your own rank."
                    End If
                Else
                    returned.Result = "Error: that rank is invalid."
                End If
            Else
                returned.Result = "Usage: /giverank [user] [rankID]"
            End If
        End If

        ' /mduty
        If cmd = "mduty" And ClientUser.CanPerformCommand(MANAGER) Then
            If Not ManagerDutyPlayers.Contains(ClientUser) Then
                returned.Result = "On Manager Duty - Good luck lol."
                ManagerDutyPlayers.Add(ClientUser)
            Else
                returned.Result = "Off duty :)"
                ManagerDutyPlayers.Remove(ClientUser)
            End If
        End If

        ' /newregister [username] [password] [rank]
        If cmd = "newregister" And ClientUser.CanPerformCommand(MANAGER) Then
            Dim targetUser As New User(args.Item(0), Net.IPAddress.Parse("127.0.0.1"), "needsToBeChanged", Nothing, args.Item(2)) With {.Password = args.Item(1)}
            If targetUser.DoesExist() = False Then
                targetUser.RegisterNew(args.Item(1))
                returned.Result = "Registered new user"
            End If
        End If

        ' /update 
        If cmd = "update" And ClientUser.CanPerformCommand(MANAGER) Then
            BannedUsers = GetBannedPlayers()
            HandleAdminStart()
            WaitForUsersUse()
            For Each usr As User In Users.Values
                usr.GetNotes() 'reloads user notes
            Next
            UsersInUse = False
            MainApplication.LatestClientVersion = GetLatestVersion(CLIENTVERSIONPATH)
            returned.Result = "Server has now reloaded details."
        End If

        ' /updateban [user] [name/serial/ip] [bool]
        If cmd = "updateban" And ClientUser.CanPerformCommand(MANAGER) Then
            If args.Count = 3 Then
                Dim _bool As Boolean = False
                Dim failed As Boolean = False
                Try
                    _bool = Convert.ToBoolean(args.Item(2))
                Catch ex As Exception
                    returned.Result = "Usage: /updateban [user] [name/serial/ip] [BOOLEAN EXPRESSION]"
                End Try
                If failed = False Then
                    Dim targetUser As String = args.Item(0)
                    Dim typeOfBan As String = args.Item(1)
                    Dim actualBan As Ban = GetBanOfUser(targetUser)
                    If actualBan IsNot Nothing Then
                        Dim isSerial As Boolean = actualBan.IsSerialBan
                        Dim isIP As Boolean = actualBan.IsIPBan
                        If typeOfBan = "name" Then
                            isSerial = False
                            isIP = False
                        ElseIf typeOfBan = "serial" Then
                            isSerial = _bool
                        ElseIf typeOfBan = "ip" Then
                            isIP = _bool
                        End If
                        UpdateBan(targetUser, isSerial, isIP)
                        returned.Result = "Ban was updated successfully."
                        returned.ResultColor = "&BLUE&"
                        BannedUsers = GetBannedPlayers() 'updates player listings.
                    Else
                        returned.Result = "Error: user was not banned"
                    End If
                End If
            Else
                returned.Result = "Usage: /updateban [user] [name/serial/ip(-ban)] [bool]"
            End If
        End If

        ' /forceserial [user]
        If cmd = "forceserial" And ClientUser.CanPerformCommand(MANAGER) Then
            ' sends a message to client to force-change their serial (will delete and then rewrite registry)
            If Users.ContainsKey(args.Item(0)) Then
                SendMessageToClient("/newserial", args.Item(0), False, True)
                returned.Result = "Sent message for user to change serial."
            Else
                returned.Result = "Error: invalid player"
            End If
        End If

        ' /m [message]
        If cmd = "m" And ClientUser.CanPerformCommand(MANAGER) Then
            If args.Count > 0 Then
                SendMessageToRank(MANAGER, "&PURPLE&" + ClientUser.Name + " mChat:" + argsAsString,, True)
                returned.DontHandle = True
            End If
        End If

        ' Commands finished
        If setUserAtEnd Then
            WaitForUsersUse()
            Users(user) = ClientUser
            UsersInUse = False
        End If
        If returned.Result = "Unknown cmd." And returned.BroadCastResult = "" Then
            ' guest or unhandled command
            If ClientUser.Rank = -1 Then
                returned.Result = "Error: you must login. Contact /admins for help"
            End If
        End If
        Return returned
    End Function

    Public Function GetBanOfUser(usr As String) As Ban
        BannedUsers = MainApplication.BannedUsers
        For Each bann As Ban In BannedUsers
            If bann.Name = usr Then
                Return bann
            End If
        Next
        Return Nothing
    End Function
    Private lastMessageTime As New DateTime
    Private recentMessages As Integer

    Friend Sub HandleLeave()
        If Users.ContainsKey(clientName) Then ' may already be removed (/kick /ban)
            WaitForUsersUse()
            Try
                SendMessageForce(Users(clientName).Client, "/quit Left_Server")
                Users(clientName).Client.Client.Shutdown(SocketShutdown.Both)
            Catch ex As Exception
            End Try
            UsersInUse = False
            RemoveUser(clientName, Users(clientName).ID) ' remove user from both list/dictionary.
        End If
        Msg(clientName + " left the server.", True)
        Broadcast(clientName + " left the server.", clientName, False)
    End Sub

    Private Sub DoChat()
        Dim infiniteCounter As Integer
        Dim requestCount As Integer
        Dim bytesFrom(65535) As Byte
        Dim dataFromClient As String
        Dim sendBytes As [Byte]()
        Dim serverResponse As String
        Dim rCount As String
        lastMessageTime = DateTime.Now
        recentMessages = 0
        requestCount = 0
        For infiniteCounter = 1 To 2
            infiniteCounter = 1
            Try
                requestCount = requestCount + 1
                Dim networkStream As NetworkStream = clientSocket.GetStream()
                networkStream.Read(bytesFrom, 0, CInt(clientSocket.ReceiveBufferSize))
                dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom)
                dataFromClient = dataFromClient.Replace(vbNullChar, String.Empty)
                If String.IsNullOrEmpty(dataFromClient) Then
                    Msg(clientName + " sent an empty message")
                    Continue For
                End If
                For Each _msg As String In dataFromClient.Split("%")
                    If String.IsNullOrEmpty(_msg) Then
                        Continue For
                    End If
                    If _msg.Contains("&Ignore&") Then
                        Continue For
                    End If
                    _msg = _msg.Substring(0, _msg.IndexOf("$$$"))
                    If _msg.Contains("&") Then recentMessages = 0
                    If (DateTime.Now - lastMessageTime).Seconds > 5 Then
                        lastMessageTime = DateTime.Now
                        recentMessages = 0
                    End If
                    If (DateTime.Now - lastMessageTime).Seconds < 5 Then
                        recentMessages += 1
                    End If
                    If recentMessages >= 4 And hasBeenMutedForFlooding Then
                        If ClientUser.IsMuted Then
                            SendMessageToClient("[Error: You are muted. You cannot send messages]" + BIGSPACE + vbCrLf + "By: " + ClientUser._Mute.Oper + vbCrLf + "For: " + ClientUser._Mute.Reason + vbCrLf + "Expires in: " + (Math.Round(ClientUser._Mute.TimeLeft.TotalSeconds, 0)).ToString, ClientUser.Name, False)
                            Continue For ' we shouldnt act on a muted client
                        End If
                        ' we should kick if user has been muted before.
                        KickUser(clientName, "flooding", "Sauron", clientSocket)
                        Exit For
                    End If
                    If recentMessages > 2 Then
                        SendMessageToClient("&RED&Stop flooding or your inputs will be blocked", clientName, False, True)
                    End If
                    If recentMessages > 4 Then
                        If ClientUser.IsMuted Then
                            KickUser(clientName, "flooding", "Sauron", clientSocket)
                            Exit For
                        End If
                        MuteUser(clientName, "flooding", "Sauron", clientSocket, 30)
                        hasBeenMutedForFlooding = True
                        Continue For
                    End If
                    If ClientUser.IsMuted Then
                        SendMessageToClient("[Error: You are muted. You cannot send messages]" + BIGSPACE + vbCrLf + "By: " + ClientUser._Mute.Oper + vbCrLf + "For: " + ClientUser._Mute.Reason + vbCrLf + "Expires in: " + (Math.Round(ClientUser._Mute.TimeLeft.TotalSeconds, 0)).ToString, ClientUser.Name, False)
                        Continue For
                    End If
                    If _msg.Substring(0, 1) = "/" Then
                        ' command
                        Dim cmdsplit As New List(Of String)(_msg.Split(" "))
                        cmdsplit.Item(0) = cmdsplit.Item(0).Remove(0, 1) ' removes the slash
                        Dim cmd As String = cmdsplit.Item(0)
                        If cmd = "dice" And (IsALLChatFrozen Or IsPublicChatFrozen) Then
                            If Not ClientUser.Rank = MANAGER Then
                                SendMessageToClient("&RED&Public chat is frozen.", clientName, False)
                                Continue For
                            End If
                        End If
                        If cmd = "a" And (IsALLChatFrozen Or IsAdminChatFrozen) Then
                            If Not ClientUser.Rank = MANAGER Then
                                SendMessageToClient("&RED&Admin chat is frozen", clientName, False)
                                Continue For
                            End If
                        End If
                        cmdsplit.RemoveAt(0)
                        Dim cmdHandled As CommandReturn = HandleCommand(clientName, cmd, cmdsplit)
                        If cmdHandled.DontHandle = False Then ' some commands will handle output on their own (eg, /help and /players)
                            If cmdHandled.BroadCastResult.Length > 0 Then
                                Broadcast(cmdHandled.BroadCastColor + cmdHandled.BroadCastResult, clientName, False)
                            End If
                            If cmdHandled.Result.Length > 0 Then
                                SendMessageToClient(cmdHandled.ResultColor + cmdHandled.Result, clientName, False)
                            End If
                        End If

                    Else
                        If IsALLChatFrozen Or IsPublicChatFrozen Then
                            SendMessageToClient("&RED&Chat is frozen.", clientName, False)
                            If Not ClientUser.Rank = MANAGER Then
                                Continue For
                            End If
                        End If
                        ' is not a command
                        If ClientUser.IsLoggedIn = False Then Continue For ' cannot send message if you are a guest.
                        If adutyUsers.Contains(clientName) Then
                            _msg = "&RED&" + _msg
                        End If
                        If _msg.Contains("&ISTYPING&") Then
                            WaitForUsersTyping()
                            If Not _UsersTyping.Contains(Me.ClientUser.Name.ToString()) Then
                                _UsersTyping.Add(Me.ClientUser.Name.ToString())
                            End If
                            UsersTypingInUse = False
                            Broadcast("&TYPING&:" & UsersTyping, "Server", False)
                            Continue For
                        ElseIf _msg.Contains("&NOTTYPING&") Then
                            WaitForUsersTyping()
                            If _UsersTyping.Contains(Me.ClientUser.Name.ToString()) Then _UsersTyping.Remove(Me.ClientUser.Name.ToString())
                            UsersTypingInUse = False
                            Broadcast("&TYPING&:" & UsersTyping, "Server", False)
                            Continue For
                        Else
                            WaitForUsersTyping()
                            If _UsersTyping.Contains(Me.ClientUser.Name.ToString()) Then _UsersTyping.Remove(Me.ClientUser.Name.ToString())
                            UsersTypingInUse = False
                            Broadcast("&TYPING&:" & UsersTyping, "Server", False)
                        End If
                        If _msg.Contains("@everyone") Then
                            If Me.ClientUser.Rank < MANAGER Then
                                _msg = _msg.Replace("@everyone", "@all")
                            End If
                        End If
                        Msg(clientName + ":/p " + _msg)
                        rCount = Convert.ToString(requestCount)
                        Broadcast(_msg, clientName, True)
                    End If
                    Threading.Thread.Sleep(3)
                Next

            Catch ex As InvalidOperationException
                Log_Outside(ex.Message + " ,,,, " + ex.StackTrace)
                HandleLeave()
                Exit For
            Catch ex As System.ArgumentOutOfRangeException
                Log_Outside(ex.Message + " ,,,, " + ex.StackTrace)
                HandleLeave()
                Exit For
            Catch ex As System.IO.IOException
                HandleLeave()
                Log_Outside("Error with client " + clientName + ": " + ex.Message)
                Exit For
            End Try
        Next
    End Sub
End Class