﻿Public Class Ban
    Public Name As String
    Public Serial As String
    Public IP As Net.IPAddress
    Public Reason As String
    Public Oper As String
    Public IsSerialBan As Boolean
    Public IsIPBan As Boolean
    ''' <summary>
    ''' Creates a new Ban, holding all information regarding a permanent ban against a player
    ''' </summary>
    ''' <param name="uName">Username of banned player</param>
    ''' <param name="uIP">IP of banned player</param>
    ''' <param name="oReason">Reason for the ban</param>
    ''' <param name="_Oper">Username of person that performed the ban (operator)</param>
    Public Sub New(uName As String, uIP As Net.IPAddress, oReason As String, _Oper As String, uSerial As String)
        Name = uName
        IP = uIP
        Reason = oReason
        Serial = uSerial
        Oper = _Oper
        IsSerialBan = False
        IsIPBan = False
    End Sub

    ''' <summary>
    ''' Boolean expression to determine whether or not the given user is banned, as per this ban.
    ''' </summary>
    ''' <param name="_user">Username to test</param>
    ''' <returns>Boolean determining true/false of banned/not banned</returns>
    Public Function IsPlayerBanned(ByVal _user As User)
        If IsSerialBan Then
            If _user.Serial = Me.Serial Then
                Return True
            End If
        End If
        If IsIPBan Then
            If _user.Serial = Me.Serial Then
                Return True
            End If
        End If
        ' check names as last resort
        If _user.Name = Me.Name Then
            Return True
        End If
        ' not banned
        Return False
    End Function
End Class
