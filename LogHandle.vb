﻿Class LogHandle
    ' Want to hold a log of messages, times, and then save them easily.
    Private _InnerLog As List(Of String)
    Private StartTime As DateTime
    Private EndTime As DateTime
    Private FilePath As String
    Private FolderPath As String
    Public LogInUse As Boolean = False
    Public Sub WaitForLogUse()
        While LogInUse = True
            Threading.Thread.Sleep(1)
        End While
        LogInUse = True
    End Sub

    Public ReadOnly Property Log As List(Of String)
        Get
            Dim _log As List(Of String) = New List(Of String)
            WaitForLogUse()
            For Each line As String In _InnerLog
                If _InnerLog.IndexOf(line) > 8 Then
                    _log.Add(line)
                End If
            Next
            LogInUse = False
            Return _log
        End Get
    End Property
    Public ReadOnly Property SavePath As String
        Get
            Return FilePath
        End Get
    End Property
    Public ReadOnly Property ReadLog As String
        Get
            Dim _log As String = ""
            WaitForLogUse()
            For Each line As String In _InnerLog
                _log += line
            Next
            LogInUse = False
            Return _log
        End Get
    End Property

    Public Sub LogMsg(msg As String, Optional overrideTime As Boolean = True)
        WaitForLogUse()
        If overrideTime = True Then
            _InnerLog.Add(msg)
        Else
            _InnerLog.Add(DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg)
        End If
        LogInUse = False
    End Sub

    Public Sub LogWarn(msg As String, Optional overrideTime As Boolean = False)
        WaitForLogUse()
        If overrideTime Then
            LogMsg("WARN: " + msg, True)
        Else
            LogMsg("WARN: " + DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg, True)
        End If
        LogInUse = False
    End Sub

    Public Sub LogError(msg As String, Optional overrideTime As Boolean = False)
        WaitForLogUse()
        If overrideTime Then
            LogMsg("ERROR: " + msg, True)
        Else
            LogMsg("ERROR: " + DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg, True)
        End If
        LogInUse = False
    End Sub

    Public Sub SaveLog(reason As String)
        EndTime = DateTime.Now()
        _InnerLog.Add("-- Closed at " + EndTime.ToString("dd-MM-yyyy hh:mm:ss.fff") + ", reason: " + reason)
        If Not IO.Directory.Exists(FolderPath) Then
            IO.Directory.CreateDirectory(FolderPath)
        End If
        System.IO.File.WriteAllLines(FilePath, _InnerLog.ToArray())
    End Sub

    Public Sub SetSerial(serial As String)
        WaitForLogUse()
        Dim temp As List(Of String) = _InnerLog
        temp.Item(2) = "Serial: " + serial
        _InnerLog = temp
        LogInUse = False
    End Sub

    Public Sub SetUser(name As String)
        WaitForLogUse()
        _InnerLog.Item(1) = "Username: " + name
        LogInUse = False
    End Sub

    Public Sub New(_path As String, _name As String, _serial As String, versionNumber As Version, _uname As String)
        StartTime = DateTime.Now()
        FilePath = _path + "log_" + StartTime.ToString().Replace(":", "-").Replace("/", "_") + ".txt"
        FolderPath = _path
        _InnerLog = New List(Of String)
        _InnerLog.Add("-- Log Init --")
        _InnerLog.Add("Username: " + _name)
        _InnerLog.Add("Serial: " + _serial)
        _InnerLog.Add("Client Version: " + versionNumber.ToString + " (" + FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion.ToString + ")")
        _InnerLog.Add("User OS: " & My.Computer.Info.OSFullName)
        _InnerLog.Add("User OS Version: " & My.Computer.Info.OSVersion)
        _InnerLog.Add("User OS Platform: " & My.Computer.Info.OSPlatform)
        _InnerLog.Add("User Name: " + _uname + " " + My.User.IsInRole(ApplicationServices.BuiltInRole.Administrator).ToString)
        _InnerLog.Add("-- Log Continues.. --")
    End Sub
End Class
