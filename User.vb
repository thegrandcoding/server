﻿Imports System.IO
Imports System.Net.Sockets
Imports System.Text
''' <summary>
''' The big one!
''' Holds all information regarding a user that is connected
''' Name: name of user
''' IP: ip they are connecting from
''' Serial: Random list of letters/symbols that is nearly unique to each computer
''' Client: TCP Client
''' Rank: -1 = Guest, 0 = User, 1 = Moderator, 2 = Administrator, 3 = Manager
''' Password: SHA256-hashed of their password
''' IsUnderCover: whether or not the player is undercover
''' IsAllowedByManager: allows for managers to join with multiple accounts (for UC purposes or something)
''' Notes: A list of all notes added by admins.
''' Punishments: A list of all punishments recorded against the user
''' Mute: the current mute that is against the user (if any)
''' </summary>
Public Class User
    Private _loggedIn As Boolean
    Private _iD As Integer
    Public Name As String
    Public IP As Net.IPAddress
    Public Serial As String
    Public Client As TcpClient
    Public ClientVersion As Version
    Public hndleClient As HandleClient
    Public Rank As Integer
    Public DoesSeePublic As Boolean = False
    Public CanRecievePM As Boolean = True
    Public Password As String
    Public IsUnderCover As Boolean = False
    Public IsAllowedByManager As Boolean
    Public Notes As List(Of AdminNote)
    Public Punishments As List(Of Punishment)
    Private Mute As Punishment
    Public ReadOnly Property _Mute As Punishment
        Get
            Return Mute
        End Get
    End Property
    Private Registration As DateTime
    Public ReadOnly Property RegistrationDate As DateTime
        Get
            Return Registration
        End Get
    End Property
    Public ReadOnly Property IsMuted As Boolean
        Get
            If Not (Mute.Target = "Server" And Mute.Oper = "Server") Then
                Return Not Mute.HasExpired
            End If
            Return False
        End Get
    End Property

    Public Sub SetRegistration(_date As String)
        If DateTime.TryParse(_date, DateTime.Now()) Then
            Registration = DateTime.Parse(_date)
        End If
    End Sub

    Public ReadOnly Property ID As Integer
        Get
            Return _iD
        End Get
    End Property
    Public ReadOnly Property IsLoggedIn As Boolean
        Get
            Return _loggedIn
        End Get
    End Property
    ReadOnly Property RankName As String
        Get
            If Rank = -1 Then Return "Guest"
            If Rank = 0 Then Return "User"
            If Rank = 1 Then Return "Moderator"
            If Rank = 2 Then Return "Admin"
            If Rank = 3 Then Return "Manager"
            If Rank = 4 Then Return "Server"
            Return "Unknown"
        End Get
    End Property

    Public ReadOnly Property DisplayName As String
        Get
            Return Name + "(" + ID.ToString + ")"
        End Get
    End Property

    Public ReadOnly Property ShortHandRank As String
        Get
            Return RankName.Substring(0, 2).ToUpper()
        End Get
    End Property

    ''' <summary>
    ''' Searches through the user save file to determine if the user exists
    ''' </summary>
    ''' <returns>Boolean, true = user is saved in file, false = not found</returns>
    Public Function DoesExist()
        For Each line As String In GetAllValidLines(USERSPATH)
            If String.IsNullOrEmpty(line) Then Continue For
            If line.Split(" ")(1) = Name Then Return True
        Next
        Return False
    End Function

    Public ReadOnly Property StoredRank As Integer
        Get
            If DoesExist() Then
                For Each line As String In GetAllValidLines(USERSPATH)
                    If String.IsNullOrEmpty(line) Then Continue For
                    ' [date] [name] [pass] [serial] [rank]
                    If line.Split(" ")(1) = Name Then
                        Return Convert.ToInt32(line.Split(" ")(4))
                    End If
                Next
            End If
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property StoredPass As String
        Get
            If DoesExist() Then
                For Each line As String In GetAllValidLines(USERSPATH)
                    If String.IsNullOrEmpty(line) Then Continue For
                    If line.Split(" ")(1) = Name Then
                        Return line.Split(" ")(2)
                    End If
                Next
            End If
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property StoredRegister As String
        Get
            If DoesExist() Then
                For Each line As String In GetAllValidLines(USERSPATH)
                    If String.IsNullOrEmpty(line) Then Continue For
                    If line.Split(" ")(1) = Name Then
                        Return line.Split(" ")(0).Replace("_", " ")
                    End If
                Next
            End If
            Return Nothing
        End Get
    End Property

    ''' <summary>
    ''' Tries to login the user with the given details
    ''' </summary>
    ''' <param name="attemptName">Username of player that is trying to login</param>
    ''' <param name="notEncrypt_attemptPass">UNENCRYPTED password</param>
    ''' <returns>Boolean, true = has logged in correctly, false = is NOT logged in</returns>
    Public Function TryLogin(attemptName As String, notEncrypt_attemptPass As String)
        ' [user] [password-sha256] [serial] [rank]
        If _loggedIn = True Then Return True
        Dim _storedPass As String = StoredPass
        Dim _storedRank As String = StoredRank
        Dim _storedRegister As String = StoredRegister
        Dim encryptedPass As String = EncryptSHA256Managed(notEncrypt_attemptPass)
        If String.IsNullOrEmpty(_storedPass) Then
            Return False ' we should not register a new user in /login.
        End If
        If encryptedPass = _storedPass Then
            DoesSeePublic = True
            _loggedIn = True
            Rank = _storedRank
            Password = _storedPass
            SetRegistration(_storedRegister)
            UpdateUserStatus(Me) 're-write information, to update new IP/serial if they have now changed.
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Registers a new user with the given UNENCRYPTED password
    ''' </summary>
    ''' <param name="unEncrypt">UNENCRYPTED password of user</param>
    Public Sub RegisterNew(unEncrypt As String)
        If _loggedIn = True Then Return
        Dim encrpytedPass As String = EncryptSHA256Managed(unEncrypt)
        Password = encrpytedPass
        DoesSeePublic = True
        _loggedIn = True
        Rank = 0
        Registration = DateTime.Now()
        File.AppendAllText(USERSPATH, Registration.ToString.Replace(" ", "_") + " " + Name + " " + Password + " " + Serial + " 0" + vbCrLf) 'starts as a User.
        Msg("Registered new user: " + Name + "(" + ID.ToString + ")")
    End Sub

    Public Function CanPerformCommand(minRank As Integer) As Boolean
        Return Rank >= minRank
    End Function

    Public Function DisplayAllNotes(Optional doubleClick As Boolean = True) As String
        Dim str As String = ""
        If doubleClick Then str = "[Double click to see " + Notes.Count.ToString + " notes]" + BIGSPACE + vbCrLf
        For Each note As AdminNote In Notes
            str += note.ToString() + vbCrLf
        Next
        If Notes.Count = 0 Then str = "There are no notes."
        Return str
    End Function

    Public Sub GetNotes()
        ' ID.ToString + ": " + IssueDate.ToString + ", " + Message + ", " + Oper
        ' [id] [date] [message] [oper]
        Dim _notes As List(Of AdminNote) = New List(Of AdminNote)
        Dim path As String = "UserSaves\" + Name + "\notes.txt"
        If File.Exists(path) Then
            For Each line As String In File.ReadAllLines(path)
                If SkipLine(line) Then Continue For
                Dim lSplit As String() = line.Split(" ")
                Dim id As Integer = Convert.ToInt32(lSplit(0))
                Dim _date As DateTime = DateTime.Parse(lSplit(1).Replace("_", " "))
                Dim message As String = lSplit(2).Replace("&", " ")
                Dim oper As String = lSplit(3)
                Dim newNote As New AdminNote(Name, oper, _date, message) With {.ID = id}
                _notes.Add(newNote)
            Next
        End If
        Notes = _notes
    End Sub

    Public Sub AddNote(msg As String, oper As String)
        Dim note As New AdminNote(Name, oper, DateTime.Now(), msg, Notes)
        Notes.Add(note)
        Dim lines As List(Of String) = New List(Of String)
        For Each _note As AdminNote In Notes
            lines.Add(_note.ToStringFile())
        Next
        Dim path As String = "UserSaves\" + Name + "\notes.txt"
        If Not File.Exists(path) Then
            File.Create(path)
        End If
        File.WriteAllLines(path, lines.ToArray())
    End Sub

    Public Sub DeletePlayer()
        Dim oldText As String() = File.ReadAllLines(USERSPATH)
        For Each line As String In oldText
            If String.IsNullOrEmpty(line) Then Continue For
            Dim lineSplit As String() = line.Split(" ")
            Dim index As Integer = Array.IndexOf(oldText, line)
            If lineSplit(1) = Name Then
                oldText(index) = "" 'removes line.
                Exit For
            End If
        Next
        Dim linelist As New List(Of String)
        For Each line As String In oldText
            If line.Trim <> "" Then linelist.Add(line)
        Next
        File.WriteAllLines(USERSPATH, linelist.ToArray)
    End Sub

    ''' <summary>
    ''' Punishes a player
    ''' </summary>
    ''' <param name="oper">Operator, admin that performed command</param>
    ''' <param name="reason">Reason for the punishment</param>
    ''' <param name="type">Only valid ones: "Kick" / "Mute" / "Warn" / "Ban"</param>
    ''' <param name="duration">How long until the mute expires. All in seconds</param>
    Public Sub Punish(oper As String, reason As String, type As String, Optional duration As Integer = 0)
        Dim puns As New Punishment(Name, oper, reason, type, duration)
        If type = "Mute" Then
            Me.Mute = puns
        End If
        If type = "Tban" Then
            MainApplication.TBannedUsers.Add(puns)
        End If
        Me.Punishments.Add(puns)
        WriteAllPunishments(Me)
    End Sub
    Dim rnd As New Random
    Private Sub AssignID()
        Dim Ids As List(Of Integer) = MainApplication.AvailableIds
        _iD = Ids.Item(0)
        Ids.RemoveAt(0)
        MainApplication.AvailableIds = Ids
    End Sub

    ''' <summary>
    ''' Creates a new user
    ''' </summary>
    ''' <param name="_name">Username of user</param>
    ''' <param name="_ip">IP of user</param>
    ''' <param name="_serial">Near unique identifier of user computer</param>
    ''' <param name="_client">TCP Client</param>
    ''' <param name="_rank">Rank of user</param>
    ''' <param name="isUC">Defaults to false. Is under cover</param>
    Public Sub New(_name As String, _ip As Net.IPAddress, _serial As String, _client As TcpClient, _rank As Integer, Optional isUC As Boolean = False)
        Name = _name
        IP = _ip
        Serial = _serial
        Client = _client
        Rank = _rank
        _loggedIn = False
        AssignID() 'gets new ID for user.
        IsAllowedByManager = False
        Punishments = ReadAllPunishments(Name)
        Mute = New Punishment("Server", "Server", "Server", "Mute")
        IsUnderCover = isUC
        GetNotes()
        ClientVersion = New Version("0.0.0.0")
    End Sub

    Private _BlockedUsers As New List(Of User)
    Public Function CanRecievePMFrom(usr As User) As Boolean
        Return Not _BlockedUsers.Contains(usr)
    End Function

    Public Function BlockUser(usr As User) As Boolean
        If Not _BlockedUsers.Contains(usr) Then
            _BlockedUsers.Add(usr)
            Return True
        Else
            _BlockedUsers.Remove(usr)
            Return False
        End If
    End Function

End Class
