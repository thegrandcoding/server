﻿Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO

Public Class Test_GUI
    Public DisplayUsers As Dictionary(Of String, User)
    Public serverUser As User
    Private SelectedUser As User = Nothing
    Private UserFilter As String = "All"
    Private _Name As String
    Private PanelLocation As Point
    Private OriginalSize As New Size(925, 420)
    Private HiddenPanels As List(Of Panel)
    Private ShowedPanel As Panel

    Private CurrentBan As Ban

    Private Function DoCommand(cmd As String) As CommandReturn
        Dim returned As CommandReturn
        If cmd.Substring(0, 1) = "/" Then cmd = cmd.Remove(0, 1)
        Dim commandSplit As String() = cmd.Split(" ")
        Dim cmdName As String = commandSplit(0)
        Dim cmdList As List(Of String) = New List(Of String)
        For Each item As String In commandSplit
            If Array.IndexOf(commandSplit, item) = 0 Then Continue For
            cmdList.Add(item)
        Next
        returned = serverUser.hndleClient.HandleCommand("Server", cmdName, cmdList)
        If returned.DontHandle = False Then
            GUIAddNewMessage("> " + returned.Result)
        End If
        UpdateAllGUI()
        Return returned
    End Function

    Public Sub GUIAddNewMessage(msg As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() GUIAddNewMessage(msg))
            Return
        End If
        lv_Log.Items.Add(msg)
    End Sub

    Public Sub StartGUI()
        If Me.InvokeRequired() Then
            Me.Invoke(Sub() StartGUI())
            Return
        End If
        Me.Enabled = True
        Me.Show()
        HiddenPanels = New List(Of Panel)
        _Name = "Server GUI"
        DisplayUsers = MainApplication.Users
        serverUser = DisplayUsers.Item("Server")
        usr_displayPanel.Hide()
        lv_Log.Columns(0).Width = lv_Log.Width - 10
        Btn_FilterAll.PerformClick()
        PanelLocation = panel_PlayersOnline.Location
        panel_Chat.Location = PanelLocation
        panel_ban.Location = PanelLocation
        panel_options.Location = PanelLocation
        panel_ban.Hide()
        panel_Chat.Hide() ' hides their uhly movement at start
        panel_options.Hide()
        HidePanel(panel_ban)
        HidePanel(panel_Chat)
        HidePanel(panel_options)
        ShowPanel(panel_PlayersOnline)
        Me.Size = OriginalSize
        Me.PanelTimer.Start()
    End Sub

    Private Sub Test_GUI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Enabled = False
        Me.Hide()
    End Sub

    Private Sub UpdateUserGUI()
        Dim showGuests As Boolean = True
        Dim showPlayers As Boolean = True
        Btn_FilterAdmins.Show()
        Btn_FilterAll.Show()
        Btn_FilterLogged.Show()
        If UserFilter = "All" Then
            Btn_FilterAll.Hide()
        ElseIf UserFilter = "Players" Then
            Btn_FilterLogged.Hide()
            showGuests = False
        Else
            Btn_FilterAdmins.Hide()
            showGuests = False
            showPlayers = False
        End If
        lb_Users.Items.Clear()
        lb_Users.Items.Add("ID  Name")
        For Each usr As User In DisplayUsers.Values
            If usr.Rank = -1 And showGuests = False Then Continue For
            If usr.Rank = 0 And showPlayers = False Then Continue For
            If usr.Name = "Server" Then Continue For
            If usr.Rank = -1 Then
                lb_Users.Items.Add("#" + usr.ID.ToString + "   " + usr.Name)
            Else
                lb_Users.Items.Add(usr.ID.ToString + "   " + usr.Name)
            End If
        Next
        Me.Text = _Name + " | Online: " + (DisplayUsers.Values.Count - 1).ToString + " | IP Address: " + MainApplication.ServerIP.ToString
    End Sub

    Public Sub UpdateAllGUI()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf UpdateAllGUI))
            Return
        End If
        Dim selectedIndex As Integer = lb_Users.SelectedIndex
        DisplayUsers = MainApplication.Users
        UpdateUserGUI()
        Try
            lb_Users.SelectedIndex = selectedIndex
        Catch ex As System.ArgumentOutOfRangeException
            ' user was probably removed/disconnected
        End Try
        ' System.Windows.Forms.ListViewItem
        Try
            lv_Log.EnsureVisible(lv_Log.Items.Count - 1)
        Catch ex As Exception
        End Try
        UpdateListedBans()
    End Sub

    Private punishSelIndex As Integer = 0
    Private Sub UpdateUserSelected()
        Dim selIndex As Integer = lb_Users.SelectedIndex
        ' ID  Name     0
        If selIndex > 0 Then
            Dim selectedItem As String = lb_Users.SelectedItem
            Dim itemSplit As String() = selectedItem.Split(" ")
            Dim userName As String = itemSplit(3)
            SelectedUser = DisplayUsers(userName)
            usr_displayPanel.Show()
            lbl_name.Text = "Name: " + SelectedUser.Name
            lbl_rank.Text = "Rank: " + SelectedUser.RankName
            lbl_serial.Text = "Serial: " + SelectedUser.Serial
            lbl_pswd.Text = "Pass: " + SelectedUser.Password
            lbl_ip.Text = "IP Address: " + SelectedUser.IP.ToString
            lb_punishments.Items.Clear()
            lb_punishments.Items.Add("Date         Type        Reason      Operator (Duration)")
            For Each pnsh As Punishment In SelectedUser.Punishments
                Dim toAdd As String = pnsh.Time.ToShortDateString + " " + pnsh.Type + " " + pnsh.Reason + " " + pnsh.Oper
                If pnsh.Duration > 0 Then toAdd += " " + pnsh.Duration.ToString
                lb_punishments.Items.Add(toAdd)
            Next
            lb_punishments.SelectedIndex = punishSelIndex
            BtnRankInc.Enabled = CanIncrease(SelectedUser.Name)
            BtnRankDec.Enabled = CanDecrease(SelectedUser.Name)
        Else
            usr_displayPanel.Hide()
            SelectedUser = Nothing
        End If
    End Sub

    Private Sub lb_Users_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lb_Users.SelectedIndexChanged
        DisplayUsers = MainApplication.Users
        UpdateUserSelected()
    End Sub

    Private Function CanDoCommand(Optional requireDuration As Boolean = False) As MsgReturn
        Dim returned As MsgReturn = New MsgReturn(False, "Unknown error.")
        Dim durationS As String = txt_Duration.Text
        Dim reason As String = txt_Reason.Text
        If requireDuration = False Then durationS = "0"
        If SelectedUser IsNot Nothing Then
            If Not String.IsNullOrEmpty(durationS) Then
                If Not String.IsNullOrEmpty(reason) Then
                    ' valid.
                    If Integer.TryParse(durationS, 60) = True Then
                        ' duration is correct
                        returned.Success = True
                    Else
                        returned.Message = "Your duration is invalid"
                    End If
                Else
                    returned.Message = "Your reason is empty"
                End If
            Else
                returned.Message = "Your duration is empty"
            End If
        Else
            returned.Message = "You have no selected user (how did you click this?)"
        End If
        Return returned
    End Function

    Private Sub Btn_Mute_Click(sender As Object, e As EventArgs) Handles Btn_Mute.Click
        Dim durationS As String = txt_Duration.Text
        Dim reason As String = txt_Reason.Text
        Dim canDoThingy As MsgReturn = CanDoCommand(True)
        If canDoThingy.Success Then
            Dim returned As CommandReturn = DoCommand("/mute " + SelectedUser.Name + " " + durationS + " " + reason)
            txt_Duration.Text = ""
            txt_Reason.Text = ""
        Else
            MsgBox(canDoThingy.Message)
        End If
    End Sub

    Private Sub Btn_Kick_Click(sender As Object, e As EventArgs) Handles Btn_Kick.Click
        Dim reason As String = txt_Reason.Text
        Dim canDoThingy As MsgReturn = CanDoCommand()
        If canDoThingy.Success Then
            Dim returned As CommandReturn = DoCommand("/kick " + SelectedUser.Name + " " + reason)
            txt_Duration.Text = ""
            txt_Reason.Text = ""
        Else
            MsgBox(canDoThingy.Message)
        End If
    End Sub

    Private Sub Btn_Warn_Click(sender As Object, e As EventArgs) Handles Btn_Warn.Click
        Dim reason As String = txt_Reason.Text
        Dim canDoThingy As MsgReturn = CanDoCommand()
        If canDoThingy.Success Then
            Dim returned As CommandReturn = DoCommand("/warn " + SelectedUser.Name + " " + reason)
            txt_Duration.Text = ""
            txt_Reason.Text = ""
        Else
            MsgBox(canDoThingy.Message)
        End If
    End Sub

    Private Sub Btn_Ban_Click(sender As Object, e As EventArgs) Handles Btn_Ban.Click
        Dim reason As String = txt_Reason.Text
        Dim canDoThingy As MsgReturn = CanDoCommand()
        If canDoThingy.Success Then
            MainApplication.BanUser(SelectedUser.Name, reason, "Server", SelectedUser.Client)
            Dim returned As CommandReturn = DoCommand("/ban " + SelectedUser.Name + " " + reason)
            txt_Duration.Text = ""
            txt_Reason.Text = ""
        Else
            MsgBox(canDoThingy.Message)
        End If
    End Sub

    Private Sub Btn_FilterAll_Click(sender As Object, e As EventArgs) Handles Btn_FilterAll.Click
        UserFilter = "All"
        UpdateAllGUI()
    End Sub

    Private Sub Btn_FilterLogged_Click(sender As Object, e As EventArgs) Handles Btn_FilterLogged.Click
        UserFilter = "Players"
        UpdateAllGUI()
    End Sub

    Private Sub Btn_FilterAdmins_Click(sender As Object, e As EventArgs) Handles Btn_FilterAdmins.Click
        UserFilter = "Admins"
        UpdateAllGUI()
    End Sub

    Private Sub lb_punishments_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lb_punishments.SelectedIndexChanged
        punishSelIndex = lb_punishments.SelectedIndex
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Btn_HandleBans.Click
        If MainApplication.BannedUsers.Count = 0 And MainApplication.TBannedUsers.Count = 0 Then
            MsgBox("There are no banned players")
            Return
        End If
        HidePanel(ShowedPanel)
        ShowPanel(panel_ban)
        UpdateListedBans()
        lb_banUsers.SelectedIndex = 0
    End Sub

    Private Sub Btn_DoServCommand_Click(sender As Object, e As EventArgs) Handles Btn_DoServCommand.Click
        Dim cmd As String = txt_ServCmd.Text
        If String.IsNullOrEmpty(cmd) Then
            MsgBox("Your command is empty!")
            Return
        End If
        If Not cmd.Substring(0, 1) = "/" Then cmd = "/" + cmd
        Dim returned As CommandReturn = DoCommand(cmd)
        txt_ServCmd.Text = ""
        txt_ServCmd.Focus()
        UpdateAllGUI()
    End Sub

    Private Sub lb_punishments_DoubleClick(sender As Object, e As EventArgs) Handles lb_punishments.DoubleClick
        Dim selitem As String = lb_punishments.SelectedItem
        Dim itemSplit As String() = selitem.Split(" ")
        Dim stringDisplay As String = "Easier-to-read-format:" + vbCrLf
        stringDisplay += "Date:          " + itemSplit(0) + vbCrLf
        stringDisplay += "Type:          " + itemSplit(1) + vbCrLf
        stringDisplay += "Reason:     " + itemSplit(2) + vbCrLf
        stringDisplay += "Operator:  " + itemSplit(3) + vbCrLf
        If itemSplit.Length >= 5 Then
            stringDisplay += "Duration:  " + itemSplit(4) + "(secs)"
        End If
        MsgBox(stringDisplay)
    End Sub

    Private Sub HidePanel(pnl As Panel)
        HiddenPanels.Add(pnl)
    End Sub

    Private Sub ShowPanel(pnl As Panel)
        If HiddenPanels.Contains(pnl) Then HiddenPanels.Remove(pnl)
        ShowedPanel = pnl
        pnl.Show()
    End Sub

    Private Sub Btn_SelOnline_Click(sender As Object, e As EventArgs) Handles Btn_SelOnline.Click
        HidePanel(ShowedPanel)
        ShowPanel(panel_PlayersOnline)
    End Sub

    Private Sub Btn_SelChat_Click(sender As Object, e As EventArgs) Handles Btn_SelChat.Click
        HidePanel(ShowedPanel)
        ShowPanel(panel_Chat)
    End Sub

    Private Sub PanelTimer_Tick(sender As Object, e As EventArgs) Handles PanelTimer.Tick
        If lb_Users.SelectedIndex <= -1 Then lb_Users.SelectedIndex = 0
        If ShowedPanel IsNot Nothing Then
            If ShowedPanel.Location.X > PanelLocation.X Then
                Dim loc As Point = ShowedPanel.Location
                ShowedPanel.Location = New Point(loc.X - 5, loc.Y)
            End If
        End If
        Dim removePanel As Panel = Nothing
        For Each HiddenPanel As Panel In HiddenPanels
            If HiddenPanel IsNot Nothing Then
                If HiddenPanel.Location.X < 1000 Then
                    Dim loc As Point = HiddenPanel.Location
                    HiddenPanel.Location = New Point(loc.X + 5, loc.Y)
                    removePanel = Nothing
                Else
                    removePanel = HiddenPanel
                End If
            End If
        Next
        If removePanel IsNot Nothing Then HiddenPanels.Remove(removePanel)
    End Sub

    Public Function GetBanOfUser(usr As String) As Ban
        BannedUsers = MainApplication.BannedUsers
        For Each bann As Ban In BannedUsers
            If bann.Name = usr Then
                Return bann
            End If
        Next
        Return Nothing
    End Function
    Public Sub UpdateListedBans()
        BannedUsers = MainApplication.BannedUsers
        Dim selIndex As Integer = lb_banUsers.SelectedIndex
        lb_banUsers.Items.Clear()
        lb_banUsers.Items.Add("Username         IP          Banned by")
        For Each usr As Ban In BannedUsers
            lb_banUsers.Items.Add(usr.Name + " " + usr.IP.ToString + " " + usr.Oper.ToString)
        Next
        For Each usr As Punishment In TBannedUsers
            lb_banUsers.Items.Add("Temp: " + usr.Target + " " + usr.Duration.ToString + " expires: " + usr.TimeLeft.TotalSeconds.ToString)
        Next

        Try
            lb_banUsers.SelectedIndex = selIndex
        Catch ex As ArgumentOutOfRangeException
            lb_banUsers.SelectedIndex = 0
        End Try
    End Sub

    Private Sub Btn_MakeName_Click(sender As Object, e As EventArgs) Handles Btn_MakeName.Click
        DoCommand("/updateban " + CurrentBan.Name + " name true")
    End Sub

    Private Sub Btn_SerialBan_Click(sender As Object, e As EventArgs) Handles Btn_SerialBan.Click
        If Btn_SerialBan.Text.Contains("Enable") Then
            DoCommand("/updateban " + CurrentBan.Name + " serial true")
        Else
            DoCommand("/updateban " + CurrentBan.Name + " serial false")
        End If
    End Sub

    Private Sub Btn_Ipban_Click(sender As Object, e As EventArgs) Handles Btn_Ipban.Click
        If Btn_Ipban.Text.Contains("Enable") Then
            DoCommand("/updateban " + CurrentBan.Name + " ip true")
        Else
            DoCommand("/updateban " + CurrentBan.Name + " ip false")
        End If
    End Sub

    Private Sub Btn_Unban_Click(sender As Object, e As EventArgs) Handles Btn_Unban.Click
        Dim oldText As String() = File.ReadAllLines(MainApplication.BANNEDPATH)
        For Each line As String In File.ReadAllLines(MainApplication.BANNEDPATH)
            If Not IsLineValid(line) Then Continue For
            If line.Split(" ")(2) = CurrentBan.Name Then
                Dim index As Integer = Array.IndexOf(oldText, line)
                Dim check As Integer = MsgBox("Are you sure that you wish to unban " + CurrentBan.Name + "?", MsgBoxStyle.YesNo)
                If check = vbYes Then
                    oldText(index) = ""
                    HandleAChat("User " + CurrentBan.Name + " was unbanned (by Server)", "Server")
                    lb_banUsers.SelectedIndex = 0
                End If
                Exit For
            End If
        Next
        File.WriteAllLines(BANNEDPATH, oldText)
        CurrentBan = Nothing
        MainApplication.BannedUsers = GetBannedPlayers() 'refresh bans
        UpdateListedBans()
        UpdateAllGUI()
    End Sub

    Public Sub UpdateUser(usr As Ban)
        lbl_ban_name.Text = "Name: " + usr.Name
        lbl_ban_oper.Text = "Operator: " + usr.Oper 'mhm, the alignment makes me happy
        lbl_ban_reason.Text = "Reason: " + usr.Reason
        lbl_ban_ip.Text = "IP Address: " + usr.IP.ToString
        lbl_ban_serial.Text = "Serial: " + usr.Serial
        If usr.IsSerialBan Then
            lbl_ban_serial.BackColor = Color.Red
            Btn_SerialBan.Text = "Disable Serial Ban"
        Else
            lbl_ban_serial.BackColor = Color.LightGreen
            Btn_SerialBan.Text = "Enable Serial Ban"
        End If
        If usr.IsIPBan Then
            lbl_ban_ip.BackColor = Color.Red
            Btn_Ipban.Text = "Disable IP Ban"
        Else
            lbl_ban_ip.BackColor = Color.LightGreen
            Btn_Ipban.Text = "Enable IP Ban"
        End If
        If usr.IsIPBan = False And usr.IsSerialBan = False Then
            Btn_MakeName.Enabled = False ' because it is name-based already
        Else
            Btn_MakeName.Enabled = True
        End If
        CurrentBan = usr
        UpdatePunishments(usr)
    End Sub

    Public Sub UpdatePunishments(usr As Ban)
        Dim punishes As List(Of Punishment) = ReadAllPunishments(usr.Name)
        Dim selIndex As Integer = lb_punishments.SelectedIndex
        If Not selIndex >= 0 Then
            selIndex = 0 'not set previously
        End If
        lb_punishments.Items.Clear()
        lb_punishments.Items.Add("Date             Type  Operator   Reason")
        For Each pns As Punishment In punishes
            lb_punishments.Items.Add(pns.Time.ToShortDateString + " " + pns.Type + " " + pns.Oper + " " + pns.Reason)
        Next
        lb_punishments.SelectedIndex = selIndex
    End Sub

    Private Sub lb_banUsers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lb_banUsers.SelectedIndexChanged
        Dim selItem As String = lb_banUsers.SelectedItem
        If selItem.Substring(0, 5) = "Temp:" Then
            Dim tBannedPun As Punishment
            selItem = selItem.Remove(0, "Temp: ".Length)
            Dim selSplit As String() = selItem.Split(" ")
            Dim tBannedUserName As String = selSplit(0)
            For Each puns As Punishment In TBannedUsers
                If puns.Target = tBannedUserName Then
                    tBannedPun = puns
                End If
            Next
            If tBannedPun IsNot Nothing Then
                MsgBox("TempBan: " + vbCrLf + "Banned player: " + tBannedPun.Target + vbCrLf + "Operator: " + tBannedPun.Oper + vbCrLf + "Reason: " + tBannedPun.Reason + vbCrLf + "Duration: " + (tBannedPun.Duration / 60).ToString + " minutes " + vbCrLf + "Time left: " + Math.Round(tBannedPun.TimeLeft.TotalSeconds).ToString + " seconds")
                Dim doParden As Integer = MsgBox("Do you wish to pardon the tban against " + tBannedPun.Target + "?", MsgBoxStyle.YesNo)
                If doParden = vbYes Then
                    TBannedUsers.Remove(tBannedPun)
                    lb_banUsers.SelectedIndex = 0
                End If
            End If
            Return
        End If
        If lb_banUsers.SelectedIndex > 0 Then
            panel_banusr.Show()
            Dim user As String = selItem.Split(" ")(0)
            UpdateUser(GetBanOfUser(user))
        Else
            panel_banusr.Hide()
        End If
    End Sub

    Private Sub Test_GUI_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        MsgBox("Closing...")
        OutSide_SaveLog("ClosingGUI")
    End Sub

    Private Sub Btn_TBan_Click(sender As Object, e As EventArgs) Handles Btn_TBan.Click
        Dim reason As String = txt_Reason.Text
        Dim duration As String = txt_Duration.Text
        Dim canDoThingy As MsgReturn = CanDoCommand(True)
        If canDoThingy.Success Then
            If MainApplication.ShowTBanWarn = True Then
                Dim checkDuration As Integer = MsgBox("Warning!" + vbCrLf + "Temp-bans are measured in MINUTES, please check your duration: do you intend to ban someone for " + duration + " minutes?", MsgBoxStyle.YesNo)
                If checkDuration = vbNo Then
                    txt_Duration.Focus()
                    Return
                End If
            End If
            MainApplication.TBanUser(SelectedUser.Name, reason, "Server", duration, SelectedUser.Client)
            Dim returned As CommandReturn = DoCommand("/tban " + SelectedUser.Name + " " + duration + " " + reason)
            txt_Duration.Text = ""
            txt_Reason.Text = ""
        Else
            MsgBox(canDoThingy.Message)
        End If
    End Sub

    Private Sub UpdateOptions()
        disregardCBChange = True
        cb_AllowCorrupt.Checked = AllowCorruptClients
        cb_AllowMultiple.Checked = AllowMultipleClients
        cb_ShowWarnT.Checked = ShowTBanWarn
        cb_AllowRegistration.Checked = AllowRegistration
        cb_FreezeChat.Checked = IsALLChatFrozen
        disregardCBChange = False
    End Sub

    Private Sub BtnOptions_Click(sender As Object, e As EventArgs) Handles BtnOptions.Click
        HidePanel(ShowedPanel)
        ShowPanel(panel_options)
        UpdateOptions()
    End Sub

    Private disregardCBChange As Boolean = False

    Private Sub cb_CheckedChanged(sender As Object, e As EventArgs) Handles cb_AllowMultiple.CheckedChanged, cb_AllowCorrupt.CheckedChanged, cb_ShowWarnT.CheckedChanged, cb_AllowRegistration.CheckedChanged, cb_FreezeChat.CheckedChanged
        If disregardCBChange Then Return
        Dim cbDone As CheckBox = sender
        If cbDone.Name.Contains("AllowMultiple") Then
            AllowMultipleClients = cbDone.Checked
            SetOption("AllowMultiple", cbDone.Checked)
        ElseIf cbDone.Name.Contains("AllowCorrupt") Then
            AllowCorruptClients = cbDone.Checked
            SetOption("AllowCorrupt", cbDone.Checked)
        ElseIf cbDone.Name.Contains("ShowWarnT") Then
            ShowTBanWarn = cbDone.Checked
            SetOption("TBanWarn", cbDone.Checked)
        ElseIf cbDone.Name.Contains("Registration") Then
            AllowRegistration = cbDone.Checked
            SetOption("AllowRegistration", cbDone.Checked)
        ElseIf cbDone.Name.Contains("Freeze") Then
            IsALLChatFrozen = cbDone.Checked
        End If
    End Sub

    Public Function CanIncrease(usrName As String) As Boolean
        If Users(usrName).Rank = -1 Then
            BtnRegisterNew.Visible = True
            BtnResetPass.Visible = True
            Return False
        Else
            BtnRegisterNew.Visible = False
            BtnResetPass.Visible = False
        End If
        Return ChangeRankBy(usrName, 1, True)
    End Function

    Public Function CanDecrease(usrName As String) As Boolean
        Return ChangeRankBy(usrName, -1, True)
    End Function

    Public Sub RegisterUser(usr As User)
        Dim userPass As String = ""
        Try
            userPass = InputBox("Please enter the user's password", "Password prompt")
        Catch ex As Exception
            userPass = ""
            Log_Outside("Error occured when trying to get user pass input")
        End Try
        If String.IsNullOrEmpty(userPass) = False Then
            usr.RegisterNew(userPass)
        Else
            MsgBox("Error: you enter a password, or entered an incorrect one.")
        End If
    End Sub

    Public Function ChangeRankBy(usrName As String, amnt As Integer, Optional justCheck As Boolean = False) As Boolean
        Dim actualUser As User
        Try
            actualUser = Users(usrName)
        Catch ex As Exception
            Log_Outside("Could not get player")
            MsgBox("Unable to get player: perhaps they disconnected?")
            Return False
        End Try
        If actualUser.Rank + amnt > 3 Then
            ' invalid
            If justCheck = False Then
                Log_Outside("Rank was too high")
                MsgBox("Unable to change rank: too high" + vbCrLf + "Valid options: 0 to 3")
            End If
            Return False
        End If
        If actualUser.Rank + amnt < 0 Then
            If justCheck = False Then
                Log_Outside("Rank was too low")
                MsgBox("Unable to change rank: too low" + vbCrLf + "Valid options: 0 to 3")
            End If
            Return False
        End If

        If actualUser.Rank = -1 And amnt >= 1 Then
            If justCheck = False Then RegisterUser(actualUser)
            Return True
        End If
        If actualUser.Rank = 0 And amnt <= -1 Then
            If justCheck = False Then MsgBox("Cannot demote past user")
            Return False
        End If
        If justCheck = False Then
            Dim difference As Integer = actualUser.Rank + amnt
            Dim returned As CommandReturn = DoCommand("/giverank " + actualUser.Name + " " + difference.ToString)
            If returned.DontHandle = False Then
                Log_Outside(returned.Result)
            End If
        End If
        Return True
    End Function

    Private Sub BtnRankInc_Click(sender As Object, e As EventArgs) Handles BtnRankInc.Click
        If CanIncrease(SelectedUser.Name) Then
            ChangeRankBy(SelectedUser.Name, 1)
        Else
            MsgBox("Unable to promote that user.")
        End If
    End Sub

    Private Sub BtnRankDec_Click(sender As Object, e As EventArgs) Handles BtnRankDec.Click
        If CanDecrease(SelectedUser.Name) Then
            ChangeRankBy(SelectedUser.Name, -1)
        Else
            MsgBox("Unable to demote that user.")
        End If
    End Sub

    Private Sub BtnRegisterNew_Click(sender As Object, e As EventArgs) Handles BtnRegisterNew.Click
        If SelectedUser.DoesExist() Then
            ' already registered
            MsgBox("This user is already registered, they must login themself")
            Return
        End If
        Dim password As String = InputBox("Please enter the user password")
        While String.IsNullOrEmpty(password)
            password = InputBox("Please enter an actual password that is not blank")
        End While
        SelectedUser.RegisterNew(password)
        SendMessageToClient("You have been registered by a server manager, double click me" + BIGSPACE + vbCrLf + "Your password is: " + password, SelectedUser.Name, False)
    End Sub

    Private Sub BtnChangeIP_Click(sender As Object, e As EventArgs) Handles BtnChangeIP.Click
        Dim confirmed As Integer = MsgBox("Changing IP address must close the server to do so." + vbCrLf + " Do you want to close the server and change IPs?", MsgBoxStyle.YesNo)
        If confirmed = vbNo Then Return
        Dim allIPs As List(Of Net.IPAddress) = GetAllAddresses()
        Dim allIPsAsString As String = "IP Addresses on this computer: " + vbCrLf
        For Each ip As Net.IPAddress In allIPs
            allIPsAsString += ip.ToString + vbCrLf
        Next
        Dim tryIP As String = InputBox("Please enter an IP address" + vbCrLf + allIPsAsString)
        If Net.IPAddress.TryParse(tryIP, ServerIP) Then
            SetOption("IP_Address", Net.IPAddress.Parse(tryIP))
            MsgBox("Server must now close for this change to take effect.." + vbCrLf + "Please re-open server in 5 seconds")
            End
        Else
            MsgBox("That is an incorrect IP address")
        End If
    End Sub

    Private Sub lv_Log_DoubleClick(sender As Object, e As EventArgs) Handles lv_Log.DoubleClick
        Dim lvi As ListViewItem = lv_Log.SelectedItems(0)
        MsgBox(lvi.Text)
    End Sub

    Private Sub BtnResetPass_Click(sender As Object, e As EventArgs) Handles BtnResetPass.Click
        If SelectedUser.DoesExist() Then
            Dim input As String = InputBox("Please enter the new password", "Pass prompt")
            Dim returned As CommandReturn = DoCommand("/resetpass " + SelectedUser.Name + " " + input)
            If returned.DontHandle = False Then
                Msg("> " + returned.Result)
                MsgBox(returned.Result)
            End If
        Else
            MsgBox("User not found")
        End If
    End Sub
End Class