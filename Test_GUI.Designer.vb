﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Test_GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lb_Users = New System.Windows.Forms.ListBox()
        Me.usr_displayPanel = New System.Windows.Forms.Panel()
        Me.BtnResetPass = New System.Windows.Forms.Button()
        Me.BtnRegisterNew = New System.Windows.Forms.Button()
        Me.BtnRankDec = New System.Windows.Forms.Button()
        Me.BtnRankInc = New System.Windows.Forms.Button()
        Me.Btn_TBan = New System.Windows.Forms.Button()
        Me.Btn_Ban = New System.Windows.Forms.Button()
        Me.Btn_Warn = New System.Windows.Forms.Button()
        Me.Btn_Kick = New System.Windows.Forms.Button()
        Me.Btn_Mute = New System.Windows.Forms.Button()
        Me.txt_Reason = New System.Windows.Forms.TextBox()
        Me.txt_Duration = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_ip = New System.Windows.Forms.Label()
        Me.lbl_pswd = New System.Windows.Forms.Label()
        Me.lb_punishments = New System.Windows.Forms.ListBox()
        Me.lbl_serial = New System.Windows.Forms.Label()
        Me.lbl_rank = New System.Windows.Forms.Label()
        Me.lbl_name = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_FilterAll = New System.Windows.Forms.Button()
        Me.Btn_FilterLogged = New System.Windows.Forms.Button()
        Me.Btn_FilterAdmins = New System.Windows.Forms.Button()
        Me.lv_Log = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txt_ServCmd = New System.Windows.Forms.TextBox()
        Me.Btn_DoServCommand = New System.Windows.Forms.Button()
        Me.panel_Chat = New System.Windows.Forms.Panel()
        Me.panel_PlayersOnline = New System.Windows.Forms.Panel()
        Me.PanelTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Btn_SelOnline = New System.Windows.Forms.Button()
        Me.Btn_SelChat = New System.Windows.Forms.Button()
        Me.Btn_HandleBans = New System.Windows.Forms.Button()
        Me.panel_ban = New System.Windows.Forms.Panel()
        Me.panel_banusr = New System.Windows.Forms.Panel()
        Me.Btn_Unban = New System.Windows.Forms.Button()
        Me.Btn_Ipban = New System.Windows.Forms.Button()
        Me.Btn_SerialBan = New System.Windows.Forms.Button()
        Me.Btn_MakeName = New System.Windows.Forms.Button()
        Me.lbl_ban_ip = New System.Windows.Forms.Label()
        Me.lbl_ban_oper = New System.Windows.Forms.Label()
        Me.lb_banPunishments = New System.Windows.Forms.ListBox()
        Me.lbl_ban_serial = New System.Windows.Forms.Label()
        Me.lbl_ban_reason = New System.Windows.Forms.Label()
        Me.lbl_ban_name = New System.Windows.Forms.Label()
        Me.lb_banUsers = New System.Windows.Forms.ListBox()
        Me.BtnOptions = New System.Windows.Forms.Button()
        Me.panel_options = New System.Windows.Forms.Panel()
        Me.cb_FreezeChat = New System.Windows.Forms.CheckBox()
        Me.cb_AllowRegistration = New System.Windows.Forms.CheckBox()
        Me.cb_ShowWarnT = New System.Windows.Forms.CheckBox()
        Me.cb_AllowCorrupt = New System.Windows.Forms.CheckBox()
        Me.cb_AllowMultiple = New System.Windows.Forms.CheckBox()
        Me.BtnChangeIP = New System.Windows.Forms.Button()
        Me.usr_displayPanel.SuspendLayout()
        Me.panel_Chat.SuspendLayout()
        Me.panel_PlayersOnline.SuspendLayout()
        Me.panel_ban.SuspendLayout()
        Me.panel_banusr.SuspendLayout()
        Me.panel_options.SuspendLayout()
        Me.SuspendLayout()
        '
        'lb_Users
        '
        Me.lb_Users.FormattingEnabled = True
        Me.lb_Users.ItemHeight = 16
        Me.lb_Users.Location = New System.Drawing.Point(-1, 38)
        Me.lb_Users.Name = "lb_Users"
        Me.lb_Users.Size = New System.Drawing.Size(273, 388)
        Me.lb_Users.TabIndex = 0
        '
        'usr_displayPanel
        '
        Me.usr_displayPanel.Controls.Add(Me.BtnResetPass)
        Me.usr_displayPanel.Controls.Add(Me.BtnRegisterNew)
        Me.usr_displayPanel.Controls.Add(Me.BtnRankDec)
        Me.usr_displayPanel.Controls.Add(Me.BtnRankInc)
        Me.usr_displayPanel.Controls.Add(Me.Btn_TBan)
        Me.usr_displayPanel.Controls.Add(Me.Btn_Ban)
        Me.usr_displayPanel.Controls.Add(Me.Btn_Warn)
        Me.usr_displayPanel.Controls.Add(Me.Btn_Kick)
        Me.usr_displayPanel.Controls.Add(Me.Btn_Mute)
        Me.usr_displayPanel.Controls.Add(Me.txt_Reason)
        Me.usr_displayPanel.Controls.Add(Me.txt_Duration)
        Me.usr_displayPanel.Controls.Add(Me.Label3)
        Me.usr_displayPanel.Controls.Add(Me.Label2)
        Me.usr_displayPanel.Controls.Add(Me.Label1)
        Me.usr_displayPanel.Controls.Add(Me.lbl_ip)
        Me.usr_displayPanel.Controls.Add(Me.lbl_pswd)
        Me.usr_displayPanel.Controls.Add(Me.lb_punishments)
        Me.usr_displayPanel.Controls.Add(Me.lbl_serial)
        Me.usr_displayPanel.Controls.Add(Me.lbl_rank)
        Me.usr_displayPanel.Controls.Add(Me.lbl_name)
        Me.usr_displayPanel.Location = New System.Drawing.Point(281, 6)
        Me.usr_displayPanel.Name = "usr_displayPanel"
        Me.usr_displayPanel.Size = New System.Drawing.Size(632, 360)
        Me.usr_displayPanel.TabIndex = 1
        '
        'BtnResetPass
        '
        Me.BtnResetPass.Location = New System.Drawing.Point(263, 22)
        Me.BtnResetPass.Name = "BtnResetPass"
        Me.BtnResetPass.Size = New System.Drawing.Size(103, 23)
        Me.BtnResetPass.TabIndex = 18
        Me.BtnResetPass.Text = "Reset Pass"
        Me.BtnResetPass.UseVisualStyleBackColor = True
        '
        'BtnRegisterNew
        '
        Me.BtnRegisterNew.Location = New System.Drawing.Point(182, 22)
        Me.BtnRegisterNew.Name = "BtnRegisterNew"
        Me.BtnRegisterNew.Size = New System.Drawing.Size(75, 23)
        Me.BtnRegisterNew.TabIndex = 17
        Me.BtnRegisterNew.Text = "Register"
        Me.BtnRegisterNew.UseVisualStyleBackColor = True
        '
        'BtnRankDec
        '
        Me.BtnRankDec.Location = New System.Drawing.Point(152, 22)
        Me.BtnRankDec.Name = "BtnRankDec"
        Me.BtnRankDec.Size = New System.Drawing.Size(24, 23)
        Me.BtnRankDec.TabIndex = 16
        Me.BtnRankDec.Text = "-"
        Me.BtnRankDec.UseVisualStyleBackColor = True
        '
        'BtnRankInc
        '
        Me.BtnRankInc.Location = New System.Drawing.Point(122, 22)
        Me.BtnRankInc.Name = "BtnRankInc"
        Me.BtnRankInc.Size = New System.Drawing.Size(24, 23)
        Me.BtnRankInc.TabIndex = 15
        Me.BtnRankInc.Text = "+"
        Me.BtnRankInc.UseVisualStyleBackColor = True
        '
        'Btn_TBan
        '
        Me.Btn_TBan.Location = New System.Drawing.Point(444, 158)
        Me.Btn_TBan.Name = "Btn_TBan"
        Me.Btn_TBan.Size = New System.Drawing.Size(88, 29)
        Me.Btn_TBan.TabIndex = 14
        Me.Btn_TBan.Text = "TempBan"
        Me.Btn_TBan.UseVisualStyleBackColor = True
        '
        'Btn_Ban
        '
        Me.Btn_Ban.Location = New System.Drawing.Point(538, 158)
        Me.Btn_Ban.Name = "Btn_Ban"
        Me.Btn_Ban.Size = New System.Drawing.Size(88, 29)
        Me.Btn_Ban.TabIndex = 13
        Me.Btn_Ban.Text = "Ban"
        Me.Btn_Ban.UseVisualStyleBackColor = True
        '
        'Btn_Warn
        '
        Me.Btn_Warn.Location = New System.Drawing.Point(256, 158)
        Me.Btn_Warn.Name = "Btn_Warn"
        Me.Btn_Warn.Size = New System.Drawing.Size(88, 29)
        Me.Btn_Warn.TabIndex = 12
        Me.Btn_Warn.Text = "Warn"
        Me.Btn_Warn.UseVisualStyleBackColor = True
        '
        'Btn_Kick
        '
        Me.Btn_Kick.Location = New System.Drawing.Point(350, 158)
        Me.Btn_Kick.Name = "Btn_Kick"
        Me.Btn_Kick.Size = New System.Drawing.Size(88, 29)
        Me.Btn_Kick.TabIndex = 11
        Me.Btn_Kick.Text = "Kick"
        Me.Btn_Kick.UseVisualStyleBackColor = True
        '
        'Btn_Mute
        '
        Me.Btn_Mute.Location = New System.Drawing.Point(162, 158)
        Me.Btn_Mute.Name = "Btn_Mute"
        Me.Btn_Mute.Size = New System.Drawing.Size(88, 29)
        Me.Btn_Mute.TabIndex = 10
        Me.Btn_Mute.Text = "Mute"
        Me.Btn_Mute.UseVisualStyleBackColor = True
        '
        'txt_Reason
        '
        Me.txt_Reason.Location = New System.Drawing.Point(406, 102)
        Me.txt_Reason.Name = "txt_Reason"
        Me.txt_Reason.Size = New System.Drawing.Size(220, 22)
        Me.txt_Reason.TabIndex = 2
        '
        'txt_Duration
        '
        Me.txt_Duration.Location = New System.Drawing.Point(516, 130)
        Me.txt_Duration.Name = "txt_Duration"
        Me.txt_Duration.Size = New System.Drawing.Size(110, 22)
        Me.txt_Duration.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(335, 133)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(175, 17)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Enter duration (if needed):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(301, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 17)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Enter Reason:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Red
        Me.Label1.ForeColor = System.Drawing.SystemColors.Control
        Me.Label1.Location = New System.Drawing.Point(433, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 17)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Punish Player:"
        '
        'lbl_ip
        '
        Me.lbl_ip.AutoSize = True
        Me.lbl_ip.Location = New System.Drawing.Point(3, 100)
        Me.lbl_ip.Name = "lbl_ip"
        Me.lbl_ip.Size = New System.Drawing.Size(84, 17)
        Me.lbl_ip.TabIndex = 5
        Me.lbl_ip.Text = "IP Address: "
        '
        'lbl_pswd
        '
        Me.lbl_pswd.AutoSize = True
        Me.lbl_pswd.Location = New System.Drawing.Point(3, 75)
        Me.lbl_pswd.Name = "lbl_pswd"
        Me.lbl_pswd.Size = New System.Drawing.Size(47, 17)
        Me.lbl_pswd.TabIndex = 4
        Me.lbl_pswd.Text = "Pass: "
        '
        'lb_punishments
        '
        Me.lb_punishments.FormattingEnabled = True
        Me.lb_punishments.ItemHeight = 16
        Me.lb_punishments.Location = New System.Drawing.Point(6, 193)
        Me.lb_punishments.Name = "lb_punishments"
        Me.lb_punishments.Size = New System.Drawing.Size(623, 164)
        Me.lb_punishments.TabIndex = 1
        '
        'lbl_serial
        '
        Me.lbl_serial.AutoSize = True
        Me.lbl_serial.Location = New System.Drawing.Point(3, 50)
        Me.lbl_serial.Name = "lbl_serial"
        Me.lbl_serial.Size = New System.Drawing.Size(52, 17)
        Me.lbl_serial.TabIndex = 2
        Me.lbl_serial.Text = "Serial: "
        '
        'lbl_rank
        '
        Me.lbl_rank.AutoSize = True
        Me.lbl_rank.Location = New System.Drawing.Point(3, 25)
        Me.lbl_rank.Name = "lbl_rank"
        Me.lbl_rank.Size = New System.Drawing.Size(113, 17)
        Me.lbl_rank.TabIndex = 1
        Me.lbl_rank.Text = "Rank: [Manager]"
        '
        'lbl_name
        '
        Me.lbl_name.AutoSize = True
        Me.lbl_name.Location = New System.Drawing.Point(3, 0)
        Me.lbl_name.Name = "lbl_name"
        Me.lbl_name.Size = New System.Drawing.Size(53, 17)
        Me.lbl_name.TabIndex = 0
        Me.lbl_name.Text = "Name: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(280, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(129, 17)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Perform Command:"
        '
        'Btn_FilterAll
        '
        Me.Btn_FilterAll.Location = New System.Drawing.Point(-1, 0)
        Me.Btn_FilterAll.Name = "Btn_FilterAll"
        Me.Btn_FilterAll.Size = New System.Drawing.Size(88, 29)
        Me.Btn_FilterAll.TabIndex = 14
        Me.Btn_FilterAll.Text = "All"
        Me.Btn_FilterAll.UseVisualStyleBackColor = True
        '
        'Btn_FilterLogged
        '
        Me.Btn_FilterLogged.Location = New System.Drawing.Point(93, 0)
        Me.Btn_FilterLogged.Name = "Btn_FilterLogged"
        Me.Btn_FilterLogged.Size = New System.Drawing.Size(88, 29)
        Me.Btn_FilterLogged.TabIndex = 15
        Me.Btn_FilterLogged.Text = "Logged-In"
        Me.Btn_FilterLogged.UseVisualStyleBackColor = True
        '
        'Btn_FilterAdmins
        '
        Me.Btn_FilterAdmins.Location = New System.Drawing.Point(187, 0)
        Me.Btn_FilterAdmins.Name = "Btn_FilterAdmins"
        Me.Btn_FilterAdmins.Size = New System.Drawing.Size(88, 29)
        Me.Btn_FilterAdmins.TabIndex = 16
        Me.Btn_FilterAdmins.Text = "Admins"
        Me.Btn_FilterAdmins.UseVisualStyleBackColor = True
        '
        'lv_Log
        '
        Me.lv_Log.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lv_Log.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lv_Log.Location = New System.Drawing.Point(0, 0)
        Me.lv_Log.Name = "lv_Log"
        Me.lv_Log.Size = New System.Drawing.Size(925, 418)
        Me.lv_Log.TabIndex = 0
        Me.lv_Log.UseCompatibleStateImageBehavior = False
        Me.lv_Log.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = ""
        Me.ColumnHeader1.Width = 906
        '
        'txt_ServCmd
        '
        Me.txt_ServCmd.Location = New System.Drawing.Point(410, 9)
        Me.txt_ServCmd.Name = "txt_ServCmd"
        Me.txt_ServCmd.Size = New System.Drawing.Size(317, 22)
        Me.txt_ServCmd.TabIndex = 14
        '
        'Btn_DoServCommand
        '
        Me.Btn_DoServCommand.Location = New System.Drawing.Point(733, 7)
        Me.Btn_DoServCommand.Name = "Btn_DoServCommand"
        Me.Btn_DoServCommand.Size = New System.Drawing.Size(132, 26)
        Me.Btn_DoServCommand.TabIndex = 18
        Me.Btn_DoServCommand.Text = "Send Command"
        Me.Btn_DoServCommand.UseVisualStyleBackColor = True
        '
        'panel_Chat
        '
        Me.panel_Chat.Controls.Add(Me.lv_Log)
        Me.panel_Chat.Location = New System.Drawing.Point(936, 481)
        Me.panel_Chat.Name = "panel_Chat"
        Me.panel_Chat.Size = New System.Drawing.Size(925, 418)
        Me.panel_Chat.TabIndex = 20
        '
        'panel_PlayersOnline
        '
        Me.panel_PlayersOnline.Controls.Add(Me.Btn_FilterAll)
        Me.panel_PlayersOnline.Controls.Add(Me.usr_displayPanel)
        Me.panel_PlayersOnline.Controls.Add(Me.lb_Users)
        Me.panel_PlayersOnline.Controls.Add(Me.Btn_FilterLogged)
        Me.panel_PlayersOnline.Controls.Add(Me.Btn_FilterAdmins)
        Me.panel_PlayersOnline.Location = New System.Drawing.Point(283, 39)
        Me.panel_PlayersOnline.Name = "panel_PlayersOnline"
        Me.panel_PlayersOnline.Size = New System.Drawing.Size(918, 436)
        Me.panel_PlayersOnline.TabIndex = 21
        '
        'PanelTimer
        '
        Me.PanelTimer.Interval = 1
        '
        'Btn_SelOnline
        '
        Me.Btn_SelOnline.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Btn_SelOnline.Image = Global.ChatProgServer.My.Resources.Resources.PlayersON
        Me.Btn_SelOnline.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_SelOnline.Location = New System.Drawing.Point(12, 12)
        Me.Btn_SelOnline.Name = "Btn_SelOnline"
        Me.Btn_SelOnline.Size = New System.Drawing.Size(262, 99)
        Me.Btn_SelOnline.TabIndex = 22
        Me.Btn_SelOnline.Text = "Players online"
        Me.Btn_SelOnline.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_SelOnline.UseVisualStyleBackColor = False
        '
        'Btn_SelChat
        '
        Me.Btn_SelChat.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Btn_SelChat.Image = Global.ChatProgServer.My.Resources.Resources.ChatImage
        Me.Btn_SelChat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_SelChat.Location = New System.Drawing.Point(12, 117)
        Me.Btn_SelChat.Name = "Btn_SelChat"
        Me.Btn_SelChat.Size = New System.Drawing.Size(262, 99)
        Me.Btn_SelChat.TabIndex = 19
        Me.Btn_SelChat.Text = "Chat / Log"
        Me.Btn_SelChat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_SelChat.UseVisualStyleBackColor = False
        '
        'Btn_HandleBans
        '
        Me.Btn_HandleBans.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Btn_HandleBans.Image = Global.ChatProgServer.My.Resources.Resources.BannedImage
        Me.Btn_HandleBans.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_HandleBans.Location = New System.Drawing.Point(12, 222)
        Me.Btn_HandleBans.Name = "Btn_HandleBans"
        Me.Btn_HandleBans.Size = New System.Drawing.Size(262, 99)
        Me.Btn_HandleBans.TabIndex = 17
        Me.Btn_HandleBans.Text = "Banned Players"
        Me.Btn_HandleBans.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_HandleBans.UseVisualStyleBackColor = False
        '
        'panel_ban
        '
        Me.panel_ban.Controls.Add(Me.panel_banusr)
        Me.panel_ban.Controls.Add(Me.lb_banUsers)
        Me.panel_ban.Location = New System.Drawing.Point(12, 481)
        Me.panel_ban.Name = "panel_ban"
        Me.panel_ban.Size = New System.Drawing.Size(918, 436)
        Me.panel_ban.TabIndex = 22
        '
        'panel_banusr
        '
        Me.panel_banusr.Controls.Add(Me.Btn_Unban)
        Me.panel_banusr.Controls.Add(Me.Btn_Ipban)
        Me.panel_banusr.Controls.Add(Me.Btn_SerialBan)
        Me.panel_banusr.Controls.Add(Me.Btn_MakeName)
        Me.panel_banusr.Controls.Add(Me.lbl_ban_ip)
        Me.panel_banusr.Controls.Add(Me.lbl_ban_oper)
        Me.panel_banusr.Controls.Add(Me.lb_banPunishments)
        Me.panel_banusr.Controls.Add(Me.lbl_ban_serial)
        Me.panel_banusr.Controls.Add(Me.lbl_ban_reason)
        Me.panel_banusr.Controls.Add(Me.lbl_ban_name)
        Me.panel_banusr.Location = New System.Drawing.Point(283, 0)
        Me.panel_banusr.Name = "panel_banusr"
        Me.panel_banusr.Size = New System.Drawing.Size(632, 436)
        Me.panel_banusr.TabIndex = 5
        '
        'Btn_Unban
        '
        Me.Btn_Unban.BackColor = System.Drawing.Color.OldLace
        Me.Btn_Unban.ForeColor = System.Drawing.Color.Red
        Me.Btn_Unban.Location = New System.Drawing.Point(383, 295)
        Me.Btn_Unban.Name = "Btn_Unban"
        Me.Btn_Unban.Size = New System.Drawing.Size(113, 116)
        Me.Btn_Unban.TabIndex = 9
        Me.Btn_Unban.Text = "Unban Player"
        Me.Btn_Unban.UseVisualStyleBackColor = False
        '
        'Btn_Ipban
        '
        Me.Btn_Ipban.Location = New System.Drawing.Point(241, 356)
        Me.Btn_Ipban.Name = "Btn_Ipban"
        Me.Btn_Ipban.Size = New System.Drawing.Size(136, 55)
        Me.Btn_Ipban.TabIndex = 8
        Me.Btn_Ipban.Text = "Enable IP Ban"
        Me.Btn_Ipban.UseVisualStyleBackColor = True
        '
        'Btn_SerialBan
        '
        Me.Btn_SerialBan.Location = New System.Drawing.Point(241, 295)
        Me.Btn_SerialBan.Name = "Btn_SerialBan"
        Me.Btn_SerialBan.Size = New System.Drawing.Size(136, 55)
        Me.Btn_SerialBan.TabIndex = 7
        Me.Btn_SerialBan.Text = "Enable Serial Ban"
        Me.Btn_SerialBan.UseVisualStyleBackColor = True
        '
        'Btn_MakeName
        '
        Me.Btn_MakeName.Location = New System.Drawing.Point(125, 295)
        Me.Btn_MakeName.Name = "Btn_MakeName"
        Me.Btn_MakeName.Size = New System.Drawing.Size(113, 116)
        Me.Btn_MakeName.TabIndex = 6
        Me.Btn_MakeName.Text = "Name-based"
        Me.Btn_MakeName.UseVisualStyleBackColor = True
        '
        'lbl_ban_ip
        '
        Me.lbl_ban_ip.AutoSize = True
        Me.lbl_ban_ip.Location = New System.Drawing.Point(3, 100)
        Me.lbl_ban_ip.Name = "lbl_ban_ip"
        Me.lbl_ban_ip.Size = New System.Drawing.Size(84, 17)
        Me.lbl_ban_ip.TabIndex = 5
        Me.lbl_ban_ip.Text = "IP Address: "
        '
        'lbl_ban_oper
        '
        Me.lbl_ban_oper.AutoSize = True
        Me.lbl_ban_oper.Location = New System.Drawing.Point(3, 50)
        Me.lbl_ban_oper.Name = "lbl_ban_oper"
        Me.lbl_ban_oper.Size = New System.Drawing.Size(73, 17)
        Me.lbl_ban_oper.TabIndex = 4
        Me.lbl_ban_oper.Text = "Operator: "
        '
        'lb_banPunishments
        '
        Me.lb_banPunishments.FormattingEnabled = True
        Me.lb_banPunishments.ItemHeight = 16
        Me.lb_banPunishments.Location = New System.Drawing.Point(3, 125)
        Me.lb_banPunishments.Name = "lb_banPunishments"
        Me.lb_banPunishments.Size = New System.Drawing.Size(623, 164)
        Me.lb_banPunishments.TabIndex = 1
        '
        'lbl_ban_serial
        '
        Me.lbl_ban_serial.AutoSize = True
        Me.lbl_ban_serial.Location = New System.Drawing.Point(3, 75)
        Me.lbl_ban_serial.Name = "lbl_ban_serial"
        Me.lbl_ban_serial.Size = New System.Drawing.Size(52, 17)
        Me.lbl_ban_serial.TabIndex = 2
        Me.lbl_ban_serial.Text = "Serial: "
        '
        'lbl_ban_reason
        '
        Me.lbl_ban_reason.AutoSize = True
        Me.lbl_ban_reason.Location = New System.Drawing.Point(3, 25)
        Me.lbl_ban_reason.Name = "lbl_ban_reason"
        Me.lbl_ban_reason.Size = New System.Drawing.Size(65, 17)
        Me.lbl_ban_reason.TabIndex = 1
        Me.lbl_ban_reason.Text = "Reason: "
        '
        'lbl_ban_name
        '
        Me.lbl_ban_name.AutoSize = True
        Me.lbl_ban_name.Location = New System.Drawing.Point(3, 0)
        Me.lbl_ban_name.Name = "lbl_ban_name"
        Me.lbl_ban_name.Size = New System.Drawing.Size(53, 17)
        Me.lbl_ban_name.TabIndex = 0
        Me.lbl_ban_name.Text = "Name: "
        '
        'lb_banUsers
        '
        Me.lb_banUsers.FormattingEnabled = True
        Me.lb_banUsers.ItemHeight = 16
        Me.lb_banUsers.Location = New System.Drawing.Point(4, 0)
        Me.lb_banUsers.Name = "lb_banUsers"
        Me.lb_banUsers.Size = New System.Drawing.Size(273, 436)
        Me.lb_banUsers.TabIndex = 4
        '
        'BtnOptions
        '
        Me.BtnOptions.BackColor = System.Drawing.Color.CornflowerBlue
        Me.BtnOptions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnOptions.Location = New System.Drawing.Point(12, 327)
        Me.BtnOptions.Name = "BtnOptions"
        Me.BtnOptions.Size = New System.Drawing.Size(262, 99)
        Me.BtnOptions.TabIndex = 23
        Me.BtnOptions.Text = "Change Options"
        Me.BtnOptions.UseVisualStyleBackColor = False
        '
        'panel_options
        '
        Me.panel_options.Controls.Add(Me.cb_FreezeChat)
        Me.panel_options.Controls.Add(Me.cb_AllowRegistration)
        Me.panel_options.Controls.Add(Me.cb_ShowWarnT)
        Me.panel_options.Controls.Add(Me.cb_AllowCorrupt)
        Me.panel_options.Controls.Add(Me.cb_AllowMultiple)
        Me.panel_options.Location = New System.Drawing.Point(1207, 39)
        Me.panel_options.Name = "panel_options"
        Me.panel_options.Size = New System.Drawing.Size(654, 436)
        Me.panel_options.TabIndex = 24
        '
        'cb_FreezeChat
        '
        Me.cb_FreezeChat.AutoSize = True
        Me.cb_FreezeChat.Location = New System.Drawing.Point(3, 114)
        Me.cb_FreezeChat.Name = "cb_FreezeChat"
        Me.cb_FreezeChat.Size = New System.Drawing.Size(217, 21)
        Me.cb_FreezeChat.TabIndex = 4
        Me.cb_FreezeChat.Text = "Disable public chat messages"
        Me.cb_FreezeChat.UseVisualStyleBackColor = True
        '
        'cb_AllowRegistration
        '
        Me.cb_AllowRegistration.AutoSize = True
        Me.cb_AllowRegistration.Location = New System.Drawing.Point(3, 60)
        Me.cb_AllowRegistration.Name = "cb_AllowRegistration"
        Me.cb_AllowRegistration.Size = New System.Drawing.Size(259, 21)
        Me.cb_AllowRegistration.TabIndex = 3
        Me.cb_AllowRegistration.Text = "Allow users to register new accounts"
        Me.cb_AllowRegistration.UseVisualStyleBackColor = True
        '
        'cb_ShowWarnT
        '
        Me.cb_ShowWarnT.AutoSize = True
        Me.cb_ShowWarnT.Location = New System.Drawing.Point(3, 87)
        Me.cb_ShowWarnT.Name = "cb_ShowWarnT"
        Me.cb_ShowWarnT.Size = New System.Drawing.Size(231, 21)
        Me.cb_ShowWarnT.TabIndex = 2
        Me.cb_ShowWarnT.Text = "Show warn about t-ban duration"
        Me.cb_ShowWarnT.UseVisualStyleBackColor = True
        '
        'cb_AllowCorrupt
        '
        Me.cb_AllowCorrupt.AutoSize = True
        Me.cb_AllowCorrupt.Location = New System.Drawing.Point(3, 33)
        Me.cb_AllowCorrupt.Name = "cb_AllowCorrupt"
        Me.cb_AllowCorrupt.Size = New System.Drawing.Size(360, 21)
        Me.cb_AllowCorrupt.TabIndex = 1
        Me.cb_AllowCorrupt.Text = "Allow user to join with an incorrect serial + checksum"
        Me.cb_AllowCorrupt.UseVisualStyleBackColor = True
        '
        'cb_AllowMultiple
        '
        Me.cb_AllowMultiple.AutoSize = True
        Me.cb_AllowMultiple.Location = New System.Drawing.Point(3, 6)
        Me.cb_AllowMultiple.Name = "cb_AllowMultiple"
        Me.cb_AllowMultiple.Size = New System.Drawing.Size(260, 21)
        Me.cb_AllowMultiple.TabIndex = 0
        Me.cb_AllowMultiple.Text = "Allow user to join with multiple clients"
        Me.cb_AllowMultiple.UseVisualStyleBackColor = True
        '
        'BtnChangeIP
        '
        Me.BtnChangeIP.Location = New System.Drawing.Point(1069, 7)
        Me.BtnChangeIP.Name = "BtnChangeIP"
        Me.BtnChangeIP.Size = New System.Drawing.Size(127, 26)
        Me.BtnChangeIP.TabIndex = 25
        Me.BtnChangeIP.Text = "Change IP"
        Me.BtnChangeIP.UseVisualStyleBackColor = True
        '
        'Test_GUI
        '
        Me.AcceptButton = Me.Btn_DoServCommand
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1924, 885)
        Me.Controls.Add(Me.BtnChangeIP)
        Me.Controls.Add(Me.panel_options)
        Me.Controls.Add(Me.BtnOptions)
        Me.Controls.Add(Me.Btn_SelOnline)
        Me.Controls.Add(Me.panel_PlayersOnline)
        Me.Controls.Add(Me.panel_Chat)
        Me.Controls.Add(Me.Btn_DoServCommand)
        Me.Controls.Add(Me.Btn_SelChat)
        Me.Controls.Add(Me.Btn_HandleBans)
        Me.Controls.Add(Me.txt_ServCmd)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.panel_ban)
        Me.Name = "Test_GUI"
        Me.Text = "Test_GUI"
        Me.usr_displayPanel.ResumeLayout(False)
        Me.usr_displayPanel.PerformLayout()
        Me.panel_Chat.ResumeLayout(False)
        Me.panel_PlayersOnline.ResumeLayout(False)
        Me.panel_ban.ResumeLayout(False)
        Me.panel_banusr.ResumeLayout(False)
        Me.panel_banusr.PerformLayout()
        Me.panel_options.ResumeLayout(False)
        Me.panel_options.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lb_Users As Windows.Forms.ListBox
    Friend WithEvents usr_displayPanel As Windows.Forms.Panel
    Friend WithEvents lbl_serial As Windows.Forms.Label
    Friend WithEvents lbl_rank As Windows.Forms.Label
    Friend WithEvents lbl_name As Windows.Forms.Label
    Friend WithEvents lb_punishments As Windows.Forms.ListBox
    Friend WithEvents lbl_pswd As Windows.Forms.Label
    Friend WithEvents lbl_ip As Windows.Forms.Label
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents txt_Reason As Windows.Forms.TextBox
    Friend WithEvents txt_Duration As Windows.Forms.TextBox
    Friend WithEvents Btn_Mute As Windows.Forms.Button
    Friend WithEvents Btn_Kick As Windows.Forms.Button
    Friend WithEvents Btn_Warn As Windows.Forms.Button
    Friend WithEvents Btn_Ban As Windows.Forms.Button
    Friend WithEvents Btn_FilterAll As Windows.Forms.Button
    Friend WithEvents Btn_FilterLogged As Windows.Forms.Button
    Friend WithEvents Btn_FilterAdmins As Windows.Forms.Button
    Friend WithEvents lv_Log As Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As Windows.Forms.ColumnHeader
    Friend WithEvents Btn_HandleBans As Windows.Forms.Button
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents txt_ServCmd As Windows.Forms.TextBox
    Friend WithEvents Btn_DoServCommand As Windows.Forms.Button
    Friend WithEvents Btn_SelChat As Windows.Forms.Button
    Friend WithEvents panel_Chat As Windows.Forms.Panel
    Friend WithEvents panel_PlayersOnline As Windows.Forms.Panel
    Friend WithEvents Btn_SelOnline As Windows.Forms.Button
    Friend WithEvents PanelTimer As Windows.Forms.Timer
    Friend WithEvents panel_ban As Windows.Forms.Panel
    Friend WithEvents panel_banusr As Windows.Forms.Panel
    Friend WithEvents Btn_Unban As Windows.Forms.Button
    Friend WithEvents Btn_Ipban As Windows.Forms.Button
    Friend WithEvents Btn_SerialBan As Windows.Forms.Button
    Friend WithEvents Btn_MakeName As Windows.Forms.Button
    Friend WithEvents lbl_ban_ip As Windows.Forms.Label
    Friend WithEvents lbl_ban_oper As Windows.Forms.Label
    Friend WithEvents lb_banPunishments As Windows.Forms.ListBox
    Friend WithEvents lbl_ban_serial As Windows.Forms.Label
    Friend WithEvents lbl_ban_reason As Windows.Forms.Label
    Friend WithEvents lbl_ban_name As Windows.Forms.Label
    Friend WithEvents lb_banUsers As Windows.Forms.ListBox
    Friend WithEvents Btn_TBan As Windows.Forms.Button
    Friend WithEvents BtnOptions As Windows.Forms.Button
    Friend WithEvents panel_options As Windows.Forms.Panel
    Friend WithEvents cb_ShowWarnT As Windows.Forms.CheckBox
    Friend WithEvents cb_AllowCorrupt As Windows.Forms.CheckBox
    Friend WithEvents cb_AllowMultiple As Windows.Forms.CheckBox
    Friend WithEvents BtnRankDec As Windows.Forms.Button
    Friend WithEvents BtnRankInc As Windows.Forms.Button
    Friend WithEvents BtnRegisterNew As Windows.Forms.Button
    Friend WithEvents BtnChangeIP As Windows.Forms.Button
    Friend WithEvents cb_AllowRegistration As Windows.Forms.CheckBox
    Friend WithEvents BtnResetPass As Windows.Forms.Button
    Friend WithEvents cb_FreezeChat As Windows.Forms.CheckBox
End Class
